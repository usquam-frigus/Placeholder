# Placeholder


### Beschrijving
Studenten studiehulp voor het bijhouden van deadlines en tentamens


### Contributors
- Peter Schriever
- Ruben Buisman
- Eran Machiels
- Femke Hoornveld


### Huidige versie
0.1.0


### Gebruikersinteractie
* Registreren / inloggen (eventueel met social media)
* Gebruikersinstellingen wijzigen (e-mail, gebruikersnaam, wachtwoord, taal)
* Deadline / tentamen toevoegen op basis van datum
 * Checklist toevoegen voor todo's voor bepaalde deadline.
 * Labels toevoegen
 * Documenten toevoegen aan tentamen / deadline
* Overzichtelijke weergave deadlines / tentamens
 * Agenda view
  * Titel van panel
  * Bijvehorende datum
  * Checklist
  * Sorteerbaar op datum / label / color
 * Grid view
  * Checklist openen onclick
  * Per dag / week / maand / jaar


### Eventuele implementatie 
* Automatische reminders die een week/dag van te voren afgaan
* Studiesessies inplannen
* Koppelen met
 * Exchange
 * Schoolrooster


### Mockups


#### Grid view
![alt text](https://preview.ibb.co/jjf7i5/mockup_grid.png)


#### Agenda view

<img src="http://i66.tinypic.com/qycsg8.png" width="600"/>
<br/>
<img src="http://i67.tinypic.com/8wgn4g.png" width="600"/>


#### Cards

<img src="http://i64.tinypic.com/2epohu8.jpg" width="600"/>
<br/>
<img src="http://i65.tinypic.com/2zg5lrn.png" width="600"/>


#### Mobile application

##### Checklist view
<img src="http://i67.tinypic.com/1zf7a1l.png" height="400" width=""/><br/>

##### Agenda view
<img src="http://i64.tinypic.com/p7ldv.png" height="400" width=""/>
<img src="http://i64.tinypic.com/sfclnb.png" height="400" width=""/>


### Development commands
Install project dependencies (libraries):
```
npm install
```

Run a local webserver (and compile files using webpack, activates on save):
```
npm start
```


### Copyright and License
 Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
 Everyone is permitted to copy and distribute verbatim copies
 of this license document, but changing it is not allowed.

 For TERMS AND CONDITIONS see the license document.
