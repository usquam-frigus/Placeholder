import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchUserDetail } from '../../actions';
import {
  StyleSheet, Text, View
} from 'react-native';
import {
  Route, Switch, withRouter, Redirect, Link
} from 'react-router-native';

import GridView from '../GridView';
import Header from '../Header';
import CreationView from '../CreationView';
import RegisterView from '../RegisterView';
import LoginView from '../LoginView';
import LogoutView from '../CreationView';

class Index extends Component {
  renderProtected(user, location) {
    if (user.loggedIn !== true) {
      return (
        <Redirect to={{
          pathname: '/login',
          state: { from: location }
        }} />
      );
    }

    let {pathname} = this.props.location;
    pathname = pathname.replace(/\/+$/, ''); // remove trailing /
    const detailMatch = pathname.match(/(\/show\/([0-9]+))$/);
    return (
      <View>
        <Switch>
          {/* Show deadline detail view for any path ending with: */}
          {/* /show/:id, where :id is a numeric value */}
          { detailMatch !== null && detailMatch.length == 3 ? (
            <CreationView cardId={detailMatch[2]} />
          ) : null }
          <Route path="/" component={GridView} />
        </Switch>
      </View>
    );
  }

  render() {
    return (
      <View>
        <Switch>
          <Route path="/register" component={RegisterView} />
          <Route path="/logout" component={LogoutView} />
          <Route path="/login" component={LoginView} />
          {this.renderProtected(this.props.user, this.props.location)}
        </Switch>
      </View>
    );
  }
}

function mapStateToProps({user}) {
  return { user };
}

Index = connect(mapStateToProps, {fetchUserDetail})(Index); // react-redux
Index = withRouter(Index); // react-router-native
export default Index;
