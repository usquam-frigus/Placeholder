import React, { Component } from 'react';
import { View, Text, TextInput } from 'react-native';
import { Field, reduxForm } from 'redux-form';
import { Link, Redirect } from 'react-router-native';
import { connect } from 'react-redux';
import { attemptLogin } from '../../actions';
import { Card, Button, Divider } from 'react-native-material-ui';

import Header from '../Header';

class LoginView extends Component {
  renderField(field) {
    const { meta } = field;
    const err = meta.touched && meta.error && (
      <View style={{
        paddingTop:5, paddingBottom:5, backgroundColor: 'red'
      }}>
       <Text>{meta.error}</Text>
     </View>
    );

    return (
      <View style={{  }}>
        <Text>{field.label}</Text>
        <TextInput placeholder={field.placeholder}
          secureTextEntry={field.secure ? true : false}
          style={{ padding: 5, marginBottom: 10 }}
          {...field.input} />
        {err}
      </View>
    )
  }

  onSubmit(values) {
    console.log(values);
    this.props.attemptLogin(values);
  }

  renderError(err) {
    return (
      <View style={{
        paddingTop:5, paddingBottom:5, backgroundColor: 'red'
      }}>
        <Text>{err}</Text>
      </View>
    );
  }

  render() {
    if (this.props.user.loggedIn === true) {
      // Redirect to the default path ('/') when logged in
      return (
        <Redirect to={{
          pathname: '/',
          state: { from: this.props.location }
        }} />
      );
    }

    const {handleSubmit} = this.props;
    const {error} = this.props.user;
    return (
      <View>
        <Header text="Registreren" icon="person-add"
          action={() => this.props.history.push('/register')} />
        <Card>
          <View style={{ padding: 10 }}>
            <Field label="E-mailadres"
              name="email"
              component={this.renderField}
              placeholder="b.jansen@hotmail.com" />
            <Field label="Wachtwoord" secure
              name="password"
              component={this.renderField}
              placeholder="Habbo123" />
          </View>
          <Divider />
          <Button text="Inloggen"
            onPress={handleSubmit(this.onSubmit.bind(this))} />
        </Card>
      </View>
    );
  }
}

function mapStateToProps({user}) {
  return {user};
}

console.log("Todo: further expand the validation rules");
function validate(values) {
  const errors = {};
  if (!values.email) {
    errors.email = "Need to enter an email";
  }
  if (!values.password) {
    errors.password = "Need to enter a password";
  }
  // If errors is empty, the form is fine to submit
  return errors;
}

LoginView = reduxForm({
  validate,
  form: "UserLoginForm"
})(LoginView);

LoginView = connect(mapStateToProps, {attemptLogin})(LoginView);
export default LoginView;
