import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { Checkbox } from 'react-native-material-ui';

export default class CardOption extends Component {
  render() {
    return (
      <View>
        <Checkbox label={this.props.text}
          value={this.props.checked}
          checked={this.props.checked ? true : false} />
      </View>
    );
  }
}
