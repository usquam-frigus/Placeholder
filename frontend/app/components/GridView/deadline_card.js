import React, { Component } from 'react';
import { Text, View, StyleSheet } from 'react-native';
import { Card, Divider } from 'react-native-material-ui';
import { withRouter } from 'react-router-native';

import CardOption from './card_option';

String.prototype.trunc = String.prototype.trunc ||
  function(n) {
    return (this.length > n) ? this.substr(0, n-1) + '...' : this;
  };

class DeadlineCard extends Component {
  renderTitle(title) {
    return (
      <Text style={{ fontSize: 16 }}>
        {title.trunc(35)}
      </Text>
    );
  }

  renderDate(startDate) {
    startDate = new Date(startDate);
    const months = [
      'Jan', 'Feb', 'Mar', 'Apr',
      'Mei', 'Juni', 'Juli', 'Aug',
      'Sept', 'Okt', 'Nov', 'Dec'
    ];
    return (
      <View style={{
          flex: 1, flexDirection: 'row', justifyContent: 'flex-end'
        }}>
        <Text style={{ color: '#222' }}>
          {startDate.getDate()}&nbsp;{months[startDate.getMonth()]}
        </Text>
      </View>
    )
  }

  renderText(text) {
    return (
      <Text>
        {typeof text === "string" ? text.trunc(250) : null}
      </Text>
    );
  }

  renderCardLabels(labels) {
    if (labels === undefined) return;
    return labels.map((label, i) => {
      return (
        <Text style={{
            backgroundColor: label.color, marginBottom: 5, borderRadius: 5,
            padding: 5, marginRight: 5, fontSize: 11
          }}
          key={i}>
          {label.name}
        </Text>
      );
    });
  }

  renderCheckList(options) {
    return options.map((option, i) => {
      if (option === undefined || i >= 3) return;
      return <CardOption checked={option.isChecked}
        key={i} text={option.text} />;
    });
  }

  render() {
    const {card, history} = this.props;
    const baseUrl = this.props.match.path.replace(/([\/]+)$/, '');
    return (
      <View style={{backgroundColor: '#ccc'}}>
        <Card onPress={() => history.push(`${baseUrl}/show/${card.id}`)}>
          <View style={{
            backgroundColor: card.color, padding: 5, flexWrap: 'wrap',
            alignItems: 'flex-start', flexDirection:'row'
          }}>
            {this.renderTitle(card.title)}
            {this.renderDate(card.startDate)}
          </View>
          <Divider />
          <View style={{
            padding: 5
          }}>
            {card.options ? this.renderCheckList(card.options) : false}
            {this.renderText(card.text)}
          </View>
          {card.labels.length ? <Divider /> : null}
          <View style={{
            padding: 5, flexWrap: 'wrap', alignItems: 'flex-start',
            flexDirection:'row'
          }}>
            {this.renderCardLabels(card.labels)}
          </View>
        </Card>
      </View>
    );
  }
}

export default withRouter(DeadlineCard);
