import React, { Component } from 'react';
import {
  StyleSheet, Text, ScrollView, View
} from 'react-native';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-native';
import { fetchDeadlines, createDeadline } from '../../actions';

import DeadlineCard from './deadline_card';
import Header from '../Header';

class GridView extends Component {
  componentDidMount() {
    this.props.fetchDeadlines();
  }

  renderCards() {
    const cards = this.props.cards.sortedArr;
    if (cards === undefined) {
      return (
        <Text>Deadlines worden geladen..</Text>
      );
    }

    if (cards.length === 0) {
      return (
        <Text>
          Er zijn geen deadlines gevonden.
        </Text>
      );
    }
    return cards.map(card => {
      return <DeadlineCard key={card.id} card={card} />
    });
  }

  addDeadline() {
    return this.props.createDeadline({
      title: "Deadline Titel", startDate: new Date(), color: '#fff',
      labels: [], options: [], text: ''
    }, (deadlineId) => {
      console.log("pushing new path:", deadlineId);
      return this.props.history.push(`/show/${deadlineId}`);
    });
  }

  render() {
    return (
      <View>
        <Header text="New" icon="add" action={this.addDeadline.bind(this)} />
        <ScrollView style={styles.container}>
          {this.renderCards()}
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    marginBottom: 100,
  },
});

function mapStateToProps({deadlines}) {
  return { cards: deadlines };
}

GridView = connect(mapStateToProps, {
  fetchDeadlines, createDeadline
})(GridView);
GridView = withRouter(GridView);

export default GridView;
