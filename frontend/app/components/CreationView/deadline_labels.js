import React, { Component } from 'react';
import { Text, View, TextInput} from 'react-native';
import { connect } from 'react-redux';
import _ from 'lodash';
import { updateDeadlineLabels } from '../../actions';
import { Button } from 'react-native-material-ui';

class DeadlineLabels extends Component {
  constructor(props) {
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleRemove = this.handleRemove.bind(this);

    let {card} = props;
    if (card.labels === undefined || card.labels === null) card.labels = [];
    this.state = { id: card.id, labels: card.labels, newLabel: '' };
  }

  generateLabelColor() {
    const colors = [
      "#FFEB3B", "#FFC107", "#FF9800", "#FF5722", "#795548", "#607D8B"
    ];
    const i = Math.floor(Math.random() * colors.length-1) + 1;
    return colors[i];
  }

  handleSubmit() {
    let newLabel = this.state.newLabel.trim();
    if (newLabel === "") return;

    // add new label to component state
    // and clear the newLabel input text
    const color = this.generateLabelColor();
    const newLabels = [...this.state.labels, {id: 0, name: newLabel, color}];
    this.setState({labels: newLabels, newLabel: ""});

    // update backend with new label
    this.props.updateDeadlineLabels({id: this.state.id, labels: newLabels});
  }

  handleRemove(labelName) {
    let newLabels = this.state.labels;
    newLabels = _.filter(newLabels, (l) => l.name !== labelName);
    this.setState({labels: newLabels});

    this.props.updateDeadlineLabels({id: this.state.id, labels: newLabels});
  }

  render() {
    const labels = this.state.labels.map((l) => (
      <Button style={{
        container: {
          backgroundColor: l.color, padding: 5, marginRight: 10,
          borderRadius: 5, marginBottom: 10
        }
      }} text={l.name}
        onPress={this.handleRemove} />
    ));

    return (
      <View style={{
        backgroundColor: '#f3f3f3', padding: 10, marginLeft: 10, marginRight: 10,
        borderRadius: 8, marginBottom: 20
      }}>
        <View style={{
          flexWrap: 'wrap', alignItems: 'flex-start', flexDirection:'row'
        }}>
          {labels}
          <TextInput value={this.state.newLabel}
            style={{
              width: 100, fontSize: 14, padding: 5
            }}
            placeholder="New label"
            onSubmitEditing={this.handleSubmit}
            onChangeText={(text) => this.setState({newLabel: text})} />
        </View>
      </View>
    );
  }
}

function mapStateToProps({deadlines}) {
  return { cards: deadlines };
}

export default connect(mapStateToProps, {updateDeadlineLabels})(DeadlineLabels);
