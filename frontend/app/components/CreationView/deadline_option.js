import React, { Component } from 'react';
import {View, Text, TextInput} from 'react-native';
import { connect } from 'react-redux';
import { updateOption } from '../../actions/index';
import { Button } from 'react-native-material-ui';
import CheckBox from 'react-native-checkbox';
import _ from 'lodash';

class DeadlineOption extends Component {
  constructor(props) {
    super(props);

    this.handleCheckboxChange = this.handleCheckboxChange.bind(this);
    this.handleTextChange = this.handleTextChange.bind(this);
    let {option} = props;
    this.state = {
      id: option.id, text: option.text, isChecked: option.isChecked,
      touched: false, deadline_id: props.deadline_id
    }
  }

  handleRemove() {
    if (this.props.onRemove !== undefined) {
      this.props.onRemove(this.state.id);
    }
  }

  handleCheckboxChange(e) {
    let newState = !this.state.isChecked;
    this.setState({ isChecked: newState });
    let {id, text, deadline_id} = this.state;

    _.debounce(() => {
      this.props.updateOption({
        id, text, isChecked: newState, deadline_id
      });
    }, 300)();
  }

  handleTextChange(text) {
    let newState = text;
    let {id, isChecked, deadline_id} = this.state;
    this.setState({ text: newState });

    _.debounce(() => {
      this.props.updateOption({
        id, isChecked, text: newState, deadline_id
      });
    }, 300, {leading: true, trailing: false})();

    if (!this.state.touched) {
      this.setState({touched: true});
    }
  }

  render() {
    let closeBtn = (<Text></Text>);
    if (!(this.state.id === 0 && this.state.touched === false)) {
      closeBtn = (
        <Button icon="close" text=""
          onPress={this.handleRemove.bind(this)} />
      );
    }
    return (
      <View style={{
        padding: 5, flexWrap: 'wrap', alignItems: 'flex-start',
        flexDirection:'row'
      }}>
        <CheckBox label={''}
          checked={this.state.isChecked ? true : false}
          onChange={this.handleCheckboxChange} />
        <TextInput value={this.state.text}
          style={{
            width: '60%', fontSize: 14, padding: 5
          }}
          placeholder="Optie tekst"
          onChangeText={this.handleTextChange} />
        {closeBtn}
      </View>
    );
  }
}

export default connect(null, {updateOption})(DeadlineOption);
