import React, { Component } from 'react';
import { View, Text, TextInput } from 'react-native';
import { Link, withRouter } from 'react-router-native';
import { MdDelete, MdClose } from 'react-icons/lib/md';
import { connect } from 'react-redux';
import _ from 'lodash';
import { updateDeadline, deleteDeadline } from '../../actions';
import { Button, Dialog, DialogDefaultActions } from 'react-native-material-ui';

class DeadlineHeader extends Component {
  constructor(props) {
    super(props);

    this.handleChange = this.handleChange.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.renderDialog = this.renderDialog.bind(this);
    if (props.card === undefined) {
      this.state = {title: '', id: 0};
      return;
    }
    this.state = {
      title: props.card.title, id: props.card.id, dialog: false
    };
  }

  handleChange(text) {
    this.setState({ title: text });

    _.debounce(() => {
      this.props.updateDeadline(_.omit(this.state, 'dialog'));
    }, 300)();
  }

  handleDelete(e) {
    if (e == 'Annuleren') {
      this.setState({dialog: false});
      return;
    }

    if (e == 'Verwijderen') {
      this.props.deleteDeadline(this.state.id);
      let {pathname} = this.props;
      pathname = pathname.replace(/(\/show\/[0-9]+)$/, '');
      return this.props.history.push(pathname);
    }

    // show alert
    this.setState({dialog: true});
  }

  renderDialog() {
    return (
      <Dialog>
        <Dialog.Title>
          <Text>Weet je het zeker?</Text>
        </Dialog.Title>
        <Dialog.Content>
          <Text>
            U probeert op dit moment de deadline met titel: {this.state.title}
            , te verwijderen. Als u hier zeker over bent,
            klik dan op verwijderen.
          </Text>
        </Dialog.Content>
        <Dialog.Actions>
          <DialogDefaultActions
            actions={['Annuleren', 'Verwijderen']}
            onActionPress={this.handleDelete} />
        </Dialog.Actions>
      </Dialog>
    );
  }

  render() {
    // remove /show/[0-9] from the current path
    let {pathname} = this.props;
    pathname = pathname.replace(/(\/show\/[0-9]+)$/, '');

    return (
      <View style={{
        padding: 5, flexWrap: 'wrap',
        alignItems: 'flex-start', flexDirection:'row'
      }}>
        <TextInput value={this.state.title}
          style={{
            width: '70%', fontSize: 14, padding: 5
          }}
          placeholder="Benoem je deadline"
          onChangeText={this.handleChange} />
          <View style={{
            flex: 1, flexDirection: 'row', justifyContent: 'flex-end'
          }}>
            <Button onPress={this.handleDelete}
              text="delete" icon="delete" />
          </View>
        <View>
          {this.state.dialog && this.renderDialog()}
        </View>
      </View>
    );
  }
}

DeadlineHeader = withRouter(DeadlineHeader);
DeadlineHeader = connect(null, {
  updateDeadline, deleteDeadline
})(DeadlineHeader);

export default DeadlineHeader;
