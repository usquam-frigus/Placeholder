import React, { Component } from 'react';
import { View, Text, TextInput} from 'react-native';
import { connect } from 'react-redux';
import { updateDeadline } from '../../actions';
import _ from 'lodash';

class DeadlineContent extends Component {
  constructor(props) {
    super(props);

    this.handleChange = this.handleChange.bind(this);
    let {card} = props;
    if (card === undefined) card = {id: 0, text: ""};
    this.state = { text: card.text, id: card.id };
  }

  handleChange(text) {
    let newState = text;
    this.setState({text: newState});
    let {id} = this.state;
    _.debounce(() => {
      this.props.updateDeadline({
        id, text: newState
      });
    }, 300)();
  }

  render() {
    return (
      <View style={{
        padding: 5, flexWrap: 'wrap', justifyContent: 'center',
        alignItems: 'flex-start', flexDirection:'row'
      }}>
        <TextInput multiline numberOfLines={8}
          style={{ width: '90%', padding: 5, paddingBottom: 10 }}
          onChangeText={this.handleChange}
          value={this.state.text} />
      </View>
    );
  }
}

export default connect(null, {updateDeadline})(DeadlineContent);
