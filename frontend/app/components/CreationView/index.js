import React, { Component } from 'react';
import { View, Text, ScrollView } from 'react-native';
import { connect } from 'react-redux';
import {
  fetchDeadlines, createDeadline, updateDeadline
} from '../../actions';
import { withRouter } from 'react-router-native';
import {
  MdColorize, MdNotifications, MdShare, MdAttachFile, MdArchive,
  MdList
} from 'react-icons/lib/md';
// import { CirclePicker } from 'react-color';
// @TODO: ColorPickers for react-native arent that good, find or
// make one ourselves for a more consistent design
import { ColorPicker, toHsv, fromHsv } from 'react-native-color-picker';
import _ from 'lodash';
import { Divider, Card, Button } from 'react-native-material-ui';

import Header from '../Header';
import DeadlineHeader from './deadline_header';
import DeadlineDateTime from './deadline_datetime';
import DeadlineChecklist from './deadline_checklist';
import DeadlineContent from './deadline_content';
import DeadlineLabels from './deadline_labels';

class CreationView extends Component {
  constructor(props) {
    super(props);

    this.toggleColorpicker = this.toggleColorpicker.bind(this);
    this.renderColorpicker = this.renderColorpicker.bind(this);
    this.updateColor = this.updateColor.bind(this);
    this.handleDone = this.handleDone.bind(this);

    let card = this.props.deadlines[this.props.cardId];
    if (card === undefined) card = {color: "#fff"};
    this.state = { showColors: false, color: toHsv(card.color) };
  }

  componentWillReceiveProps(nextProps) {
    let newCard = nextProps.deadlines[this.props.cardId];
    if (this.state.color !== toHsv(newCard.color)) {
      this.setState({color: toHsv(newCard.color)});
    }
  }

  componentDidMount() {
    if (Object.keys(this.props.deadlines).length <= 0) {
      this.props.fetchDeadlines(filter);
    }
  }

  footerButton(icon, click, disabled) {
    return (
      <Button onPress={click !== undefined ? click : null}
        disabled={disabled ? true : false}
        text="" icon={icon} />
    );
  }

  toggleColorpicker(e) {
    this.setState({showColors: !this.state.showColors});
  }

  updateColor(hsv) {
    this.setState({color: hsv, showColors: false});
    this.props.updateDeadline({id: this.props.cardId, color: fromHsv(hsv)});
  }

  handleDone(e) {
    const closePath = this.props.location.pathname
      .replace(/(\/show\/[0-9]+)$/, '');
    return this.props.history.push(closePath);
  }

  renderColorpicker() {
    return (
      <View>
        <ColorPicker style={{flex: 1}}
          value={this.state.color}
          onColorSelected={this.updateColor} />
      </View>
    );
  }

  handleBack() {
    let pathname = this.props.location.pathname
      .replace(/(\/show\/[0-9]+)$/, '');
    return this.props.history.push(pathname);
  }

  render() {
    const card = this.props.deadlines[this.props.cardId];
    if (card === undefined) {
      return (
        <View>
          <Text>Loading deadline..</Text>
        </View>
      );
    }

    return (
      <View>
        <Header text="Back" icon="arrow-back"
          action={this.handleBack.bind(this)} />
        <ScrollView style={{ marginBottom: 100 }}>
          {this.state.showColors && this.renderColorpicker()}
          <Card style={{
            backgroundColor: card.color, padding: 5, flexWrap: 'wrap',
            alignItems: 'flex-start', flexDirection:'row'
          }}>
          <DeadlineHeader card={card}
            pathname={this.props.location.pathname} />
            <Divider />
            <DeadlineDateTime card={card} />
            <Divider />
            <DeadlineChecklist card={card} />
            <Divider />
            <DeadlineContent card={card} />
            <Divider />
            <DeadlineLabels card={card} />
            <Divider />
            <View style={{
              flexWrap: 'wrap', alignItems: 'flex-start', flexDirection:'row',
              backgroundColor: "#f3f3f3", justifyContent: 'center',
              marginBottom: 20
            }}>
            {this.footerButton('share', null, true)}
            {this.footerButton('colorize', null, true)}
            {this.footerButton('notifications', null, true)}
            {this.footerButton('attach-file', null, true)}
            {this.footerButton('archive', null, true)}
          </View>
          <Button onPress={this.handleDone}
            style={{ container: { height: 40 }, text: { fontSize: 20 }}}
            primary={true}
            text="Done" />
          </Card>
        </ScrollView>
      </View>
    );
  }
}

function mapStateToProps({deadlines}) {
  return {deadlines};
}

export default withRouter(
  connect(mapStateToProps, {
    fetchDeadlines, createDeadline, updateDeadline
  })(CreationView)
);
