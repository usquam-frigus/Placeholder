import React, { Component } from 'react';
import {View, Text} from 'react-native';
import DeadlineOption from './deadline_option';
import { connect } from 'react-redux';
import _ from 'lodash';
import { deleteOption, addOption } from '../../actions';
import { Button } from 'react-native-material-ui';

class DeadlineChecklist extends Component {
  constructor(props) {
    super(props);

    let {card} = this.props;
    if (card.options === undefined || card.options === null) card.options = [];
    let options = _.mapKeys(card.options, 'id');
    this.state = { options: options, id: card.id }
  }

  handleRemove(optionId) {
    // console.log("Trying to del:", optionId, this.state.options);
    let options = _.filter(this.state.options, (o) => o.id !== optionId);
    options = _.mapKeys(options, 'id');
    this.setState({options});
    this.props.deleteOption(optionId, this.state.id);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.card === undefined) return;
    const options = nextProps.card.options;
    this.setState({options});
  }

  addOption() {
    if (this.props.disabled) return;
    this.props.addOption(this.state.id);
  }

  render() {
    let {options} = this.state;
    options = _.map(options, (o, i) => {
      if (o === undefined) return;
      return (<DeadlineOption option={o}
        deadline_id={this.state.id}
        onRemove={this.handleRemove.bind(this)}
        key={o.id} />);
    });

    return (
      <View style={{
        padding: 5, flexWrap: 'wrap', justifyContent: 'center',
        alignItems: 'flex-start', flexDirection:'row'
      }}>
        {options}
        <Button text="Add option" icon="add"
          onPress={this.addOption.bind(this)} />
      </View>
    );
  }
}

export default connect(null, {deleteOption, addOption})(DeadlineChecklist);
