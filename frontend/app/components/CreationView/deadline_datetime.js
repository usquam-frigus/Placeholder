import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { connect } from 'react-redux';
import { updateDeadline } from '../../actions';
import moment from 'moment';
import _ from 'lodash';
import DatePicker from 'react-native-datepicker';

class DeadlineDateTime extends Component {
  constructor(props) {
    super(props);

    this.handleDateChange = this.handleDateChange.bind(this);

    this.state = {
      id: props.card.id,
      startDate: moment(props.card.startDate).format("YYYY-MM-DD HH:mm"),
      focusedTime: false
    };
  }

  handleDateChange(newDate) {
    if(newDate === "") return;
    this.setState({ startDate: newDate });

    const {id} = this.state;
    _.debounce(() => {
      this.props.updateDeadline({id, startDate: moment(newDate).toDate()});
    }, 300)();
  }

  render() {
    return (
      <View style={{
        padding: 5, flexWrap: 'wrap', justifyContent: 'center',
        alignItems: 'flex-start', flexDirection:'row'
      }}>
        <DatePicker style={{ width: 200 }}
          date={this.state.startDate}
          mode="datetime"
          placeholder="select date"
          format="YYYY-MM-DD HH:mm"
          confirmBtnText="Bevestig"
          cancelBtnText="Annuleer"
          onDateChange={this.handleDateChange} />
      </View>
    );
  }
}

export default connect(null, {updateDeadline})(DeadlineDateTime);
