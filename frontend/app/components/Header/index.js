import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { Button } from 'react-native-material-ui';
import { connect } from 'react-redux';

import { createDeadline } from '../../actions';

class Header extends Component {
  constructor(props) {
    super(props);

    this.state = { add: true };
  }

  render() {
    const {viewStyle, textStyle} = styles;
    return (
      <View style={viewStyle}>
        <Text style={textStyle}>
          Placeholder
        </Text>
        <View style={{
          flex: 1, flexDirection: 'row', justifyContent: 'flex-end',
          marginRight: 10, marginTop: -5
        }}>
          <Button text={this.props.text}
            icon={this.props.icon ? this.props.icon : 'unknown'}
            onPress={this.props.action} />
        </View>
      </View>
    );
  }
}

function mapStateToProps({deadlines}) {
  return {deadlines};
}

Header = connect(mapStateToProps, {createDeadline})(Header);
export default Header;

const styles = {
  viewStyle: {
    backgroundColor: '#F8F8F8',
    flexWrap: 'wrap',
    alignItems: 'flex-start',
    flexDirection:'row',
    height: 80,
    paddingTop: 40,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,
    elevation: 2,
    position: 'relative'
  },
  textStyle: {
    fontSize: 20,
    marginLeft: 10
  }
};
