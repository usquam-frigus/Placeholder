import React, { Component } from 'react';
import {
  StyleSheet, Text, View, NativeModules
} from 'react-native';
import { NativeRouter } from 'react-router-native';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import ReduxPromise from 'redux-promise';
import { ThemeProvider, COLOR } from 'react-native-material-ui';

import Index from './components/Index';
import reducers from './reducers';

const createStoreWithMiddleware = applyMiddleware(ReduxPromise)(createStore);

const { UIManager } = NativeModules;
UIManager.setLayoutAnimationEnabledExperimental &&
  UIManager.setLayoutAnimationEnabledExperimental(true);

// you can set your style right here, it'll be propagated to application
const uiTheme = {
  palette: {
    primaryColor: COLOR.green500,
  },
  toolbar: {
    container: {
      height: 50,
    },
  },
};

export default class App extends Component {
  render() {
    return (
      <NativeRouter>
        <Provider store={createStoreWithMiddleware(reducers)}>
          <ThemeProvider uiTheme={uiTheme}>
            <Index />
          </ThemeProvider>
        </Provider>
      </NativeRouter>
    );
  }
}

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: '#fff',
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
// });
