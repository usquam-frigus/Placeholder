import {
  FETCH_USER,
  ATTEMPT_LOGIN,
  REGISTER_USER,
  LOGOUT_USER
} from '../actions/types';
import { AsyncStorage } from 'react-native';

const defaultState = {
  loggedIn: false
};

function updateAuth({api_token}, {status}) {
  try {
    const curr = AsyncStorage.getItem('@Placeholder:api_token');
    if (status === 200 && api_token !== curr) {
      AsyncStorage.setItem('@Placeholder:api_token', api_token);
    }
  } catch (err) {
    console.log(err);
  }
}

export default function(state = defaultState, action) {
  switch (action.type) {
    case FETCH_USER:
      return {
        loggedIn: action.payload.status === 200 ? true : false,
        user: action.payload.data
      };
    break;
    case ATTEMPT_LOGIN:
      if (action.payload.status === 200) {
        updateAuth(action.payload.data.user, action.payload);
      }
      return action.payload.data;
    break;
    case REGISTER_USER:
      if (action.payload.status === 400) {
        let error = false;
        if (action.payload.data.code === "ER_DUP_ENTRY") {
          error = "E-mailadres is al in gebruik.";
        }
        return {
          loggedIn: false,
          error: error !== undefined ? error : "Onbekende fout"
        };
      }
      updateAuth(action.payload.data, action.payload);
      return {
        loggedIn: action.payload.status === 200 ? true : false,
        user: action.payload.data
      };
    break;
    case LOGOUT_USER:
      if (action.payload.status == "success") {
        return { loggedIn: false };
      }
    break;
  }
  return state;
}
