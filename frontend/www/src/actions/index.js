import {
  FETCH_USER,
  LOGOUT_USER,
  ATTEMPT_LOGIN,
  UPDATE_OPTION,
  ADD_OPTION,
  DELETE_OPTION,
  DELETE_DEADLINE,
  CREATE_DEADLINE,
  UPDATE_FILTER,
  REGISTER_USER,
  FETCH_DEADLINES,
  UPDATE_DEADLINE_LABELS,
  CHANGE_APP_MODE,
  CHANGE_CAL_TYPE,
  CAL_NEXT,
  CAL_PREV,
  UPDATE_DEADLINE,
  UPDATE_DEADLINES,
  LOAD_MORE_CARDS
} from './types';
import moment from 'moment';
// import qs from 'qs';
import Cookies from 'universal-cookie';
import defaultAxios from 'axios';
const cookies = new Cookies();

const API_ROOT = 'http://localhost:3000';
// create a new instance of axios to handle HTTP status 400 (Bad Request)
// , and display client side errors
const axios = defaultAxios.create({
  validateStatus: (status) => {
    return (status >= 200 && status < 300) || status === 400;
  },
  baseURL: API_ROOT
});

export function createDeadline(deadlineData, callback) {
  const result = axios.post('/deadlines-new', deadlineData, {
    headers: { Authorization: 'Bearer ' + cookies.get('api_token') }
  }).then((response) => {
    callback(response.data.id);
    return response;
  });

  return {
    type: CREATE_DEADLINE,
    payload: result
  }
}

export function registerUser({name, email, password}) {
  const result = axios.post('/register', {
    name: name,
    email: email,
    password: password
  });

  return {
    type: REGISTER_USER,
    payload: result
  };
}

export function attemptLogin({email, password}) {
  const result = axios.post('/login', {
    email: email,
    password: password
  });

  return {
    type: ATTEMPT_LOGIN,
    payload: result
  };
}

export function logoutUser() {
  cookies.remove('api_token');
  return {
    type: LOGOUT_USER,
    payload: {
      status: "success"
    }
  };
}

export function fetchUserDetail() {
  const result = axios.get('/user-detail', {
    headers: { Authorization: 'Bearer '+cookies.get('api_token') }
  });
  result.catch((err) => {
    logoutUser(); // in case the api_token is no longer valid
  })
  return {
    type: FETCH_USER,
    payload: result
  };
}

export function updateFilter(newFilter) {
  return {
    type: UPDATE_FILTER,
    payload: newFilter
  }
}

export function fetchDeadlines(filter, search) {
  filter = filter !== undefined ? filter : {
    labels: [],
    colors: [],
    sortBy: {col: "startDate", order: "ASC"},
    matchAll: false
  };
  search = search !== undefined ? search : "";

  // Retrieve deadlines from back-end, passing through the filter options and
  // an optional searchQuery
  const result = axios.post('/deadlines', {filter, search}, {
    headers: { Authorization: 'Bearer '+cookies.get('api_token') }
  });
  return {
    type: FETCH_DEADLINES,
    payload: result
  };
}

export function updateDeadline(updateData) {
  // console.log("updateData:", updateData); // should be column: value
  if (updateData.id === 0) {
    // waiting for insert, do not send axios request
    return {type: UPDATE_DEADLINE, payload: null};
  }
  // axios request to update the deadline
  const result = axios.put(`/deadlines/${updateData.id}`, updateData, {
    headers: { Authorization: 'Bearer '+cookies.get('api_token') }
  });

  return {
    type: UPDATE_DEADLINE,
    payload: result
  }
}

export function deleteDeadline(deadline_id) {
  if (deadline_id === 0) {
    console.log("card id is 0");
    // waiting for insert, do not send axios request
    return {type: DELETE_DEADLINE, payload: null};
  }
  // axios request to update the deadline
  const result = axios.delete(`/deadlines/${deadline_id}`, {
    headers: { Authorization: 'Bearer '+cookies.get('api_token') }
  });

  return {
    type: DELETE_DEADLINE,
    payload: result
  }
}

export function updateOption(updateData) {
  // console.log("updateData:", updateData); // should be column: value
  if (updateData.id === 0) {
    // waiting for insert, do not send axios request
    return {type: UPDATE_OPTION, payload: null};
  }

  // axios request to update the option
  const result = axios.put(`/options/${updateData.id}`, updateData, {
    headers: { Authorization: 'Bearer '+cookies.get('api_token') }
  });

  return {
    type: UPDATE_OPTION,
    payload: result
  }
}

export function addOption(deadline_id) {
  const result = axios.post(
    `/options`,
    { text: "", isChecked: false, deadline_id },
    { headers: { Authorization: 'Bearer '+cookies.get('api_token') }
  });

  return {
    type: ADD_OPTION,
    payload: result
  }
}

export function deleteOption(optionId, deadline_id) {
  if (optionId === 0) {
    // option was not inserted yet, do not send axios request
    return {type: DELETE_OPTION, payload: null};
  }

  let result = axios.delete(`/options/${optionId}/${deadline_id}`, {
    headers: { Authorization: 'Bearer '+cookies.get('api_token') }
  });

  return {
    type: DELETE_OPTION,
    payload: result
  }
}

export function updateDeadlineLabels(updateData) {
  // console.log(updateData);
  // should be labels: array[{label1}, {label2}], id: 0(deadline_id)
  if (updateData.id === 0) {
    // waiting for insert, do not send axios request
    return {type: UPDATE_DEADLINE_LABELS, payload: null};
  }
  // axios request to update the option
  const result = axios.put(`/deadline-labels/${updateData.id}`, updateData, {
    headers: { Authorization: 'Bearer '+cookies.get('api_token') }
  });

  return {
    type: UPDATE_DEADLINE_LABELS,
    payload: result
  }
}
