import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { PlusButton } from 'react-svg-buttons';

import { createDeadline } from '../../actions';

class FloatingAddButton extends Component {
  handleCreate() {
    this.props.createDeadline({
      title: "Deadline Titel", startDate: new Date(), color: '#fff',
      labels: [], options: [], text: ''
    }, (deadlineId) => {
      this.props.history.push(`/show/${deadlineId}`);
    });
  }

  render() {
    return (
      <span onClick={this.handleCreate.bind(this)}>
        <PlusButton color="#222"
          className="floating-add-button"
          size={64}
          thickness={4} />
      </span>
    );
  }
}

FloatingAddButton = withRouter(FloatingAddButton);
FloatingAddButton = connect(null, {createDeadline})(FloatingAddButton);
export default FloatingAddButton;
