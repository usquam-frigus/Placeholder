import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link, Redirect, withRouter } from 'react-router-dom';
import { MdClose } from 'react-icons/lib/md';

class UserDetail extends Component {
  render() {
    const bgStyling = {
      position: 'fixed',
      top: 0,
      left: 0,
      width: '100%',
      height: '100%',
      zIndex: 50,
      background: 'rgba(34, 34, 34, 0.8)'
    }
    const popUpStyling = {
      zIndex: 100,
      position: 'fixed',
      top: 75,
      display: 'block',
      minWidth: 400,
      left: '50%',
      background: '#F8F8F8',
      transform: 'translateX(-50%)'
    };
    const {user} = this.props.user;

    // remove /user-detail from the current path
    let {pathname} = this.props.location;
    pathname = pathname.replace(/(\/user-detail)$/, '');
    return (
      <div style={bgStyling}>
        <div className="container" style={popUpStyling}>
          <div className="panel panel-default filter-panel"
            style={popUpStyling}>
            <div className="panel-heading">
              <span>
                Gebruikersinformatie
              </span>
              <Link to={pathname} className="pull-right">
                <MdClose />
              </Link>
            </div>
            <div className="panel-body">
              <div className="row">
                <label className="col-xs-4">Naam:</label>
                <span style={{ paddingLeft: 15 }}>
                  {user.name === undefined ? "Loading.." : user.name}
                </span>
              </div>
              <div className="row">
                <label className="col-xs-4">E-mailadres:</label>
                <span className="col-xs-4" style={{ paddingLeft: 15 }}>
                  {user.email === undefined ? "Loading.." : user.email}
                </span>
              </div>
            </div>
            <div className="panel-footer">
              <Link className="logout-link" to="/logout">
                Klik hier om uit te loggen
              </Link>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps({user}) {
  return {user};
}

export default withRouter(
  connect(mapStateToProps)(UserDetail)
);
