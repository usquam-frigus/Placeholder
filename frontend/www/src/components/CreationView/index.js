import React, {Component} from 'react';
import { connect } from 'react-redux';
import {
  fetchDeadlines, createDeadline, updateDeadline
} from '../../actions';
import { withRouter } from 'react-router-dom';
import {
  MdColorize, MdNotifications, MdShare, MdAttachFile, MdArchive,
  MdList
} from 'react-icons/lib/md';
import { CirclePicker } from 'react-color';
import _ from 'lodash';

import DeadlineHeader from './deadline_header';
import DeadlineDateTime from './deadline_datetime';
import DeadlineChecklist from './deadline_checklist';
import DeadlineContent from './deadline_content';
import DeadlineLabels from './deadline_labels';

class CreationView extends Component {
  constructor(props) {
    super(props);

    this.toggleColorpicker = this.toggleColorpicker.bind(this);
    this.renderColorpicker = this.renderColorpicker.bind(this);
    this.updateColor = this.updateColor.bind(this);
    this.handleDone = this.handleDone.bind(this);

    let card = this.props.deadlines[this.props.cardId];
    if (card === undefined) card = {color: "#fff"};
    this.state = { showColors: false, color: card.color };
  }

  componentWillReceiveProps(nextProps) {
    let newCard = nextProps.deadlines[this.props.cardId];
    if (this.state.color !== newCard.color) {
      this.setState({color: newCard.color});
    }
  }

  componentDidMount() {
    this.props.fetchDeadlines(this.props.filter);
  }

  footerButton(icon, click, disabled) {
    return (
      <button type="button"
        onClick={click !== undefined ? click
          : () => console.log("This feature is not implemented yet")}
        style={{ marginRight: 10, fontSize: '1.5em' }}
        disabled={disabled}
        className="btn btn-default">
        {icon}
      </button>
    )
  }

  toggleColorpicker(e) {
    this.setState({showColors: !this.state.showColors});
  }

  updateColor({hex}) {
    this.setState({color: hex, showColors: false});
    this.props.updateDeadline({id: this.props.cardId, color: hex})
  }

  handleDone(e) {
    const closePath = this.props.location.pathname
      .replace(/(\/show\/[0-9]+)$/, '');
    return this.props.history.push(closePath);
  }

  renderColorpicker() {
    return (
      <div style={{
        zIndex: 200,
        position: 'fixed',
        background: '#fff',
        border: '1px solid #999',
        padding: 10,
        borderRadius: 20,
        left: '50%',
        transform: 'translateX(-50%)',
        top: 125,
        boxShadow: '0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23)'
      }}>
        <CirclePicker onChange={this.updateColor}
          value={this.state.color} />
      </div>
    );
  }

  render() {
    const bgStyling = {
      position: 'fixed',
      top: 0,
      left: 0,
      width: '100%',
      height: '100%',
      zIndex: 50,
      background: 'rgba(34, 34, 34, 0.8)'
    }
    const popUpStyling = {
      zIndex: 100,
      position: 'fixed',
      top: 75,
      display: 'block',
      minWidth: 400,
      left: '50%',
      background: '#F8F8F8',
      padding: 0,
      transform: 'translateX(-50%)'
    };

    const card = this.props.deadlines[this.props.cardId];
    if (card === undefined) {
      return (
        <div style={bgStyling}>
          <div className="container" style={popUpStyling}>
            <div>Loading deadlines..</div>
          </div>
        </div>
      );
    }

    return (
      <div style={bgStyling}>
        <div className="container" style={popUpStyling}>
          {this.state.showColors && this.renderColorpicker()}
          <div className="panel panel-default filter-panel">
            <div className="panel-heading">
              <DeadlineHeader card={card}
                pathname={this.props.location.pathname} />
            </div>
            <div className="panel-body">
              <DeadlineDateTime card={card} />
              <DeadlineChecklist className="row"
                innerClassName="col-xs-12" card={card} />
              <div className="row">
                <div className="col-xs-12">
                  <DeadlineContent card={card} />
                </div>
              </div>
              <div>
                <DeadlineLabels card={card} />
              </div>
            </div>
            <div className="panel-footer">
              {this.footerButton(<MdColorize />, this.toggleColorpicker)}
              {this.footerButton(<MdShare />, null, true)}
              {this.footerButton(<MdNotifications />, null, true)}
              {this.footerButton(<MdAttachFile />, null, true)}
              {this.footerButton(<MdArchive />, null, true)}
              {this.footerButton(<MdList />, null, true)}

              <button type="button"
                onClick={this.handleDone}
                style={{ fontSize: '1.5em' }}
                className="btn btn-primary pull-right">
                Done
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps({deadlines, filter}) {
  return {deadlines, filter};
}

export default withRouter(
  connect(mapStateToProps, {
    fetchDeadlines, createDeadline, updateDeadline
  })(CreationView)
);
