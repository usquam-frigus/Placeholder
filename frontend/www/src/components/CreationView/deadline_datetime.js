import React, { Component } from 'react';
import { connect } from 'react-redux';
import { updateDeadline } from '../../actions';
import moment from 'moment';
import { DateField } from 'react-date-picker';
import Timepicker from 'react-timepicker';
import timepickerStyles from 'react-timepicker/timepicker.css';
import { MdAccessTime } from 'react-icons/lib/md';

class DeadlineDateTime extends Component {
  constructor(props) {
    super(props);

    this.handleDateChange = this.handleDateChange.bind(this);
    this.toggleTimepicker = this.toggleTimepicker.bind(this);
    this.handleTimeChange = this.handleTimeChange.bind(this);

    this.state = {
      id: props.card.id,
      startDate: moment(props.card.startDate).format("YYYY-MM-DD HH:mm"),
      focusedTime: false
    }
  }

  handleTimeChange(newHrs, newMins) {
    const extractDate = moment(this.state.startDate).format("YYYY-MM-DD");
    // prefix with a 0, for better datetime standards support
    newHrs = String(newHrs).length <= 1 ? String('0'+newHrs) : newHrs;
    newMins = String(newMins).length <= 1 ? String('0'+newMins) : newMins;

    let startDate = String(extractDate+" "+newHrs+":"+newMins);
    this.setState({startDate});

    const {id} = this.state;
    _.debounce(() => {
      this.props.updateDeadline({id, startDate: moment(startDate).toDate()});
    }, 300)();
  }

  handleDateChange(newDate) {
    if(newDate === "") return;
    const extractTime = moment(this.state.startDate).format("HH:mm");

    let startDate = String(newDate+" "+extractTime);
    this.setState({ startDate: startDate });

    const {id} = this.state;
    _.debounce(() => {
      this.props.updateDeadline({id, startDate: moment(startDate).toDate()});
    }, 300)();
  }

  toggleTimepicker() {
    this.setState({focusedTime: !this.state.focusedTime});
  }

  render() {
    return (
      <div style={{minHeight: 25}}>
        <div className="col-xs-6">
          <DateField dateFormat="YYYY-MM-DD"
            style={{
              border: 'none',
              background: 'none',
              color: '#555'
            }}
            onChange={this.handleDateChange}
            value={moment(this.state.startDate).format("YYYY-MM-DD")} />
        </div>
        <div className="col-xs-5 col-xs-offset-1">
          <div className="pull-right">
            <span onClick={this.toggleTimepicker}
              className="hover-darken"
              style={{
                border: 'none',
                background: 'none',
                color: '#555',
                fontSize: '1em',
                cursor: 'pointer'
              }}>
              {moment(this.state.startDate).format("HH:mm")}
              <MdAccessTime style={{
                  marginLeft: 10, marginTop: -2, fontSize: '1.2em'
                }} />
            </span>
            <div className={
                `${this.state.focusedTime ? "active" : "hidden"} timepicker`
              }
              onMouseLeave={() => this.setState({focusedTime: false})}
              style={{
                zIndex: 200,
                position: 'fixed',
                background: '#fff',
                border: '1px solid #999',
                marginLeft: -260,
                boxShadow: '0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23)'
              }}>
              <Timepicker hours={parseInt(moment(this.state.startDate).format("HH"))}
                minutes={parseInt(moment(this.state.startDate).format("mm"))}
                onChange={this.handleTimeChange} />
              </div>
            </div>
          </div>
      </div>
    );
  }
}

export default connect(null, {updateDeadline})(DeadlineDateTime);
