import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { MdDelete, MdClose } from 'react-icons/lib/md';
import { connect } from 'react-redux';
import _ from 'lodash';
import { updateDeadline, deleteDeadline } from '../../actions';

class DeadlineHeader extends Component {
  constructor(props) {
    super(props);

    this.handleChange = this.handleChange.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    if (props.card === undefined) {
      this.state = {title: '', id: 0};
      return;
    }
    this.state = { title: props.card.title, id: props.card.id };
  }

  handleChange(e) {
    this.setState({ title: e.target.value });

    _.debounce(() => {
      this.props.updateDeadline(this.state);
    }, 300)();
  }

  handleDelete(e) {
     // show alert
     let result = window.confirm(
       `You are about to delete the deadline with title:\n`
       + `${this.state.title}.\n\nAre you sure?`
     );
     if (result === true) {
       this.props.deleteDeadline(this.state.id);

       let {pathname} = this.props;
       pathname = pathname.replace(/(\/show\/[0-9]+)$/, '');
       this.props.history.push(pathname);
     }
  }

  render() {
    // remove /show/[0-9] from the current path
    let {pathname} = this.props;
    pathname = pathname.replace(/(\/show\/[0-9]+)$/, '');

    return (
      <div>
        <button onClick={this.handleDelete}
          style={{
            padding: 0, margin: 0, border: 'none', background: 'none',
            marginRight: 15
          }}>
          <MdDelete style={{ fontSize: '2em' }} />
        </button>
        <input type="text" value={this.state.title}
          style={{
            display: 'inline-block',
            width: '70%',
            background: 'none',
            border: 'none',
            fontSize: '1.25em',
            padding: 5
          }}
          placeholder="Benoem je deadline"
          onChange={this.handleChange} />
        <div className="pull-right">
          <Link to={pathname}>
            <MdClose />
          </Link>
        </div>
      </div>
    );
  }
}

DeadlineHeader = withRouter(DeadlineHeader);
DeadlineHeader = connect(null, {
  updateDeadline, deleteDeadline
})(DeadlineHeader);

export default DeadlineHeader;
