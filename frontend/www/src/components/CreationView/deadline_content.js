import React, { Component } from 'react';
import { connect } from 'react-redux';
import { updateDeadline } from '../../actions';

class DeadlineContent extends Component {
  constructor(props) {
    super(props);

    this.handleChange = this.handleChange.bind(this);
    let {card} = props;
    if (card === undefined) card = {id: 0, text: ""};
    this.state = { text: card.text, id: card.id };
  }

  handleChange(e) {
    let newState = e.target.value;
    this.setState({text: newState});
    let {id} = this.state;
    _.debounce(() => {
      this.props.updateDeadline({
        id, text: newState
      });
    }, 300)();
  }

  render() {
    return (
      <textarea value={this.state.text}
        className="hover-darken form-control"
        style={{ border: 'none', padding: 5, minHeight: 130 }}
        onChange={this.handleChange} />
    );
  }
}

export default connect(null, {updateDeadline})(DeadlineContent);
