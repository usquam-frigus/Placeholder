import React, { Component } from 'react';
import { connect } from 'react-redux';
import { MdLabel } from 'react-icons/lib/md';
import { updateDeadlineLabels } from '../../actions';

class DeadlineLabels extends Component {
  constructor(props) {
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleRemove = this.handleRemove.bind(this);

    let {card} = props;
    if (card.labels === undefined || card.labels === null) card.labels = [];
    this.state = { id: card.id, labels: card.labels, newLabel: '' };
  }

  generateLabelColor() {
    const colors = [
      "#FFEB3B", "#FFC107", "#FF9800", "#FF5722", "#795548", "#607D8B"
    ];
    const i = Math.floor(Math.random() * colors.length-1) + 1;
    return colors[i];
  }

  handleSubmit(e) {
    e.preventDefault();

    let newLabel = this.state.newLabel.trim();
    if (newLabel === "") return;

    // add new label to component state
    // and clear the newLabel input text
    const color = this.generateLabelColor();
    const newLabels = [...this.state.labels, {id: 0, name: newLabel, color}];
    this.setState({labels: newLabels, newLabel: ""});

    // update backend with new label
    this.props.updateDeadlineLabels({id: this.state.id, labels: newLabels});
  }

  handleRemove(e) {
    if (e.keyCode !== 8 || this.state.newLabel.trim() !== "") return;
    let newLabels = this.state.labels; newLabels.pop();
    this.setState({labels: newLabels});

    this.props.updateDeadlineLabels({id: this.state.id, labels: newLabels});
  }

  render() {
    const labels = this.state.labels.map((l, i) => (
      <span style={{
        background: l.color, padding: 5, marginRight: 10, borderRadius: 5,
        verticalAlign: 'sub', display: 'inline-block'
      }} key={i}>
        {l.name}
      </span>
    ));

    return (
      <div className="col-xs-12 tag-box">
        <MdLabel style={{ marginRight: 10 }} />
        {labels}
        <form onSubmit={this.handleSubmit} style={{ display: 'inline-block' }}>
          <input type="text" className="form-control"
            onChange={(e) => this.setState({newLabel: e.target.value})}
            onKeyDown={this.handleRemove}
            value={this.state.newLabel}
            style={{
              border: 'none', width: 'auto', minWidth: 100, boxShadow: 'none',
              display: 'inline-block', background: 'none', verticalAlign: 'sub',
              borderBottom: '2px dotted #999'
            }} />
        </form>
      </div>
    );
  }
}

export default connect(null, {updateDeadlineLabels})(DeadlineLabels);
