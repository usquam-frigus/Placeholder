import React, { Component } from 'react';
import { MdClose } from 'react-icons/lib/md';
import { connect } from 'react-redux';
import { updateOption } from '../../actions/index';

class DeadlineOption extends Component {
  constructor(props) {
    super(props);

    this.handleCheckboxChange = this.handleCheckboxChange.bind(this);
    this.handleTextChange = this.handleTextChange.bind(this);
    let {option} = props;
    this.state = {
      id: option.id, text: option.text, isChecked: option.isChecked,
      touched: false, deadline_id: props.deadline_id
    }
  }

  handleRemove() {
    if (this.props.onRemove !== undefined) {
      this.props.onRemove(this.state.id);
    }
  }

  handleCheckboxChange(e) {
    let newState = !this.state.isChecked;
    this.setState({ isChecked: newState });
    let {id, text, deadline_id} = this.state;

    _.debounce(() => {
      this.props.updateOption({
        id, text, isChecked: newState, deadline_id
      });
    }, 300)();
  }

  handleTextChange(e) {
    let newState = e.target.value;
    let {id, isChecked, deadline_id} = this.state;
    this.setState({ text: newState });

    _.debounce(() => {
      this.props.updateOption({
        id, isChecked, text: newState, deadline_id
      });
    }, 300, {leading: true, trailing: false})();

    if (!this.state.touched) {
      this.setState({touched: true});
    }
  }

  render() {
    let closeBtn = (<span></span>);
    if (!(this.state.id === 0 && this.state.touched === false)) {
      closeBtn = (
        <MdClose style={{
            fontSize: '1.2em', marginTop: 10, marginLeft: 5,
            cursor: 'pointer'
          }} className="hover-darken"
          onClick={this.handleRemove.bind(this)} />
      );
    }
    return (
      <div className="input-group">
        <span style={{ border: 'none', background: 'none' }}
          className="input-group-addon">
          <input type="checkbox"
            className=""
            onChange={this.handleCheckboxChange}
            checked={this.state.isChecked} />
        </span>
        <input type="text"
          placeholder={this.props.placeholder}
          className="form-control hover-darken"
          style={{
            border: 'none',
            boxShadow: 'inset 0 -1px 1px rgba(0,0,0,.075)',
            width: '90%'
          }}
          onChange={this.handleTextChange}
          value={this.state.text} />
        <div>
          {closeBtn}
        </div>
      </div>
    );
  }
}

export default connect(null, {updateOption})(DeadlineOption);
