import React, { Component } from 'react';
import DeadlineOption from './deadline_option';
import { connect } from 'react-redux';
import { MdAdd } from 'react-icons/lib/md';
import _ from 'lodash';

import { deleteOption, addOption } from '../../actions';

class DeadlineChecklist extends Component {
  constructor(props) {
    super(props);

    let {card} = this.props;
    if (card.options === undefined || card.options === null) card.options = [];
    let options = _.mapKeys(card.options, 'id');
    this.state = { options: options, id: card.id }
  }

  handleRemove(optionId) {
    // console.log("Trying to del:", optionId, this.state.options);
    let options = _.filter(this.state.options, (o) => o.id !== optionId);
    options = _.mapKeys(options, 'id');
    this.setState({options});
    this.props.deleteOption(optionId, this.state.id);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.card === undefined) return;
    const options = nextProps.card.options;
    this.setState({options});
  }

  addOption() {
    if (this.props.disabled) return;
    this.props.addOption(this.state.id);
  }

  render() {
    let {options} = this.state;
    options = _.map(options, (o, i) => {
      if (o === undefined) return;
      return (<DeadlineOption option={o}
        deadline_id={this.state.id}
        onRemove={this.handleRemove.bind(this)}
        key={o.id} />);
    });
    return (
      <div className={this.props.className}>
        <div className={this.props.innerClassName}>
          {options}
          <div style={{ cursor: 'pointer', color: '#777' }}
            className="hover-darken"
            title={this.props.disabled ? "Voeg eerst een titel toe" : null}
            onClick={this.addOption.bind(this)}>
            <MdAdd />
            <span>Add option</span>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(null, {deleteOption, addOption})(DeadlineChecklist);
