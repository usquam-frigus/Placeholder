import React, { Component } from 'react';

export default class CardOption extends Component {
  render() {
    return (
      <div className="card-option">
        <input type="checkbox"
          onClick={e => e.preventDefault()}
          defaultChecked={this.props.checked} />
        <label>{this.props.text.trunc(25)}</label>
      </div>
    );
  }
}
