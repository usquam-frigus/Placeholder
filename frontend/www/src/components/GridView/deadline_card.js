import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';
import moment from 'moment';

import CardOption from './card_option';

console.log("Todo: store String utility function seperate from GridView/deadline_card?");
String.prototype.trunc = String.prototype.trunc ||
  function(n) {
    return (this.length > n) ? this.substr(0, n-1) + '...' : this;
  };

console.log("Todo: limit DeadlineCard checkboxes");
class DeadlineCard extends Component {
  renderCardLabels(card) {
    if (card.labels === undefined) return;
    return card.labels.map((label, i) => {
      return (
        <span className="grid-card-label"
          style={{
            background: label.color, display: 'inline-block', marginBottom: 5
          }}
          key={i}>
          {label.name}
        </span>
      );
    });
  }

  renderCheckList(options) {
    return options.map((option, i) => {
      if (option === undefined) return;
      return <CardOption checked={option.isChecked}
        key={i} text={option.text} />
    });
  }

  render() {
    const {card} = this.props;

    const months = [
      'Jan', 'Feb', 'Mar', 'Apr',
      'Mei', 'Juni', 'Juli', 'Aug',
      'Sept', 'Okt', 'Nov', 'Dec'
    ];
    const contrasts = {
      "#f44336": "#fff",
      "#ffeb3b": "#000",
      "#03a9f4": "#222",
      "#00bcd4": "#111",
      "#3f51b5": "#fff",
      "#f5f5f5": "#222"
    };
    const colorStyling = {
      background: card.color,
      color: contrasts[card.color]
    };
    const cardDate = new Date(card.startDate);
    const baseUrl = this.props.match.path.replace(/([\/]+)$/, '');
    return (
      <div className="flex-child panel panel-default">
        <div className="panel-heading" style={colorStyling}>
          <Link style={{ color: colorStyling.color }}
            className="hover-darken"
            to={`${baseUrl}/show/${card.id}`}>
            <h3 className="panel-title">
              {card.title}
              <span className="pull-right grid-card-date">
                {cardDate.getDate()}&nbsp;{months[cardDate.getMonth()]}
              </span>
            </h3>
          </Link>
        </div>
        <div className="panel-body">
          {card.options ? this.renderCheckList(card.options) : false}
          {typeof card.text === "string" ? card.text.trunc(250) : null}
        </div>
        <div className="panel-footer" style={colorStyling}>
          {this.renderCardLabels(card)}
        </div>
      </div>
    );
  }
};

export default withRouter(DeadlineCard);
