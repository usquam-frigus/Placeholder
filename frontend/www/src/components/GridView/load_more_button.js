import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { loadMoreCards } from '../../actions/index';

class LoadMoreButton extends Component {
  render() {
    return (
      <button onClick={this.props.loadMoreCards}
        className="btn btn-default grid-load-more">
        Load more...
      </button>
    );
  }
}

export default connect(null, { loadMoreCards })(LoadMoreButton);
