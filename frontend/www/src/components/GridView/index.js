import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchDeadlines } from '../../actions';

import DeadlineCard from './deadline_card';
import LoadMoreButton from './load_more_button';

class GridView extends Component {
  componentDidMount() {
    this.props.fetchDeadlines(this.props.filter);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.filter !== this.props.filter) {
      this.props.fetchDeadlines(nextProps.filter);
    }
  }

  renderCards() {
    const cards = this.props.cards.sortedArr;
    if (cards === undefined || cards.length === 0) {
      return (
        <span>
          Er zijn geen deadlines gevonden.
        </span>
      );
    }
    return cards.map(card => {
      return <DeadlineCard key={card.id} card={card} />
    });
  }

  render() {
    return (
      <div className="grid-view">
        <div className="flex-container">
          {this.renderCards()}
        </div>
        {/* <LoadMoreButton /> */}
      </div>
    );
  }
}

function mapStateToProps({deadlines, filter}) {
  return { cards: deadlines, filter };
}

export default connect(mapStateToProps, { fetchDeadlines })(GridView);
