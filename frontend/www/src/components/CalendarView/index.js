import React, { Component } from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import { fetchDeadlines } from '../../actions';

import BigCalendar from 'react-big-calendar';
import calStyles from 'react-big-calendar/lib/css/react-big-calendar.css';
import moment from 'moment';

// Setup the localizer by providing the moment (or globalize) Object
// to the correct localizer.
BigCalendar.momentLocalizer(moment); // or globalizeLocalizer

class CalendarView extends Component {
  componentDidMount() {
    this.props.fetchDeadlines(this.props.filter);
  }

  handleCardSelect(card) {
    // redirect to show card-details(/edit)
    const baseUrl = this.props.match.path;
    this.props.history.push(`${baseUrl}/show/${card.id}`);
  }

  render() {
    if (this.props.cards === undefined) {
      return (
        <div>Loading deadlines..</div>
      );
    }

    let cards = _.map(this.props.cards, (card, k) => {
      if (k === "sortedArr") return { title: "", startDate: new Date() };
      card.startDate = moment(card.startDate).toDate();
      card.endDate = moment(card.startDate).add(1, 'hour').toDate();
      return card;
    });
    return (
      <div className="row col-sm-10 col-sm-offset-1">
        <div style={{ height: 700 }}>
          <BigCalendar events={cards}
            culture='en-GB'
            // defaultView={this.props.view}
            onSelectEvent={this.handleCardSelect.bind(this)}
            startAccessor='startDate'
            endAccessor='endDate' />
        </div>
      </div>
    );
  }
}

function mapStateToProps({deadlines, filter}) {
  return {
    cards: deadlines,
    filter
  };
}

export default connect(mapStateToProps, { fetchDeadlines })(CalendarView);
