import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { Link, Redirect } from 'react-router-dom';
import { registerUser } from '../../actions';
import { connect } from 'react-redux';

class RegisterView extends Component {
  renderField(field) {
    const { meta } = field;
    const err = meta.touched && meta.error && (
      <div className="alert alert-danger" style={{
        paddingTop:5,
        paddingBottom:5
      }}>
       {meta.touched ? meta.error : ''}
      </div>
    );

    return (
      <div className="col-md-12 form-group">
        <label htmlFor={field.name}>{field.label}</label>
        <input name={field.name} placeholder={field.placeholder}
          className="form-control"
          id={field.name}
          {...field.input}
          type={field.type} />
        {err}
      </div>
    )
  }

  onSubmit(values) {
    this.props.registerUser(values);
  }

  renderError(err) {
    return (
      <div className="alert alert-danger" style={{
        paddingTop:5,
        paddingBottom:5
      }}>
        {err}
      </div>
    );
  }

  render() {
    if (this.props.user.loggedIn === true) {
      // Redirect to the default path ('/') when logged in
      return (
        <Redirect to={{
          pathname: '/',
          state: { from: this.props.location }
        }} />
      );
    }

    const {handleSubmit} = this.props;
    const {error} = this.props.user;
    return (
      <div className="row">
        <div className="col-md-offset-4 col-md-4">
          <div className="panel panel-default">
            <div className="panel-heading">Registeren</div>

              <div className="panel-body">
                <form role="register"
                  onSubmit={handleSubmit(this.onSubmit.bind(this))}>
                  {error ? this.renderError(error) : null}
                  <Field label="Naam" name="name"
                    placeholder="Voornaam Achternaam"
                    type="text"
                    component={this.renderField} />
                  <Field label="E-mailadres" name="email"
                    placeholder="b.jansen@hotmail.com"
                    type="email"
                    component={this.renderField} />
                  <Field label="Wachtwoord" name="password"
                    placeholder="Habbo123"
                    type="password"
                    component={this.renderField} />
                  <div className="form-group">
                    <button type="submit"
                      className="btn btn-success btn-block" >
                      Registeren
                    </button>
                  </div>
                </form>
              </div>
              <div className="panel-footer">
                <Link to="/login">
                  Login
                </Link>
                <Link className="pull-right" to="/forgotpassword">
                  Wachtwoord vergeten?
                </Link>
              </div>
          </div>
        </div>
      </div>
    );
  }
}

console.log("Todo: further expand the validation rules");
function validate(values) {
  const errors = {};
  // validate the inputs from 'values'
  if (!values.fullName) {
    errors.fullName = "Need to enter a name";
  }
  if (!values.email) {
    errors.email = "Need to enter an email";
  }
  if (!values.password) {
    errors.password = "Need to enter a password";
  }
  // If errors is empty, the form is fine to submit
  return errors;
}

function mapStateToProps({user}) {
  return {user};
}

RegisterView = reduxForm({
  validate,
  form: "UserRegistrationForm"
})(RegisterView);

RegisterView = connect(mapStateToProps, {registerUser})(RegisterView);
export default RegisterView;
