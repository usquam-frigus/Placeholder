import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchUserDetail } from '../../actions';
import Cookies from 'universal-cookie';
import {
  Route, Switch, withRouter, Redirect
} from 'react-router-dom';

import MenuBar from '../MenuBar';
import FloatingAddButton from '../FloatingAddButton';
import GridView from '../GridView';
import CalendarView from '../CalendarView';
import LoginView from '../LoginView';
import RegisterView from '../RegisterView';
import FilterView from '../FilterView';
import UserDetail from '../UserDetail';
import LogoutView from '../LogoutView';
import CreationView from '../CreationView';

class App extends Component {
  renderOverlays(pathname, match) {
    pathname = pathname.replace(/\/+$/, ''); // remove trailing /

    const detailMatch = pathname.match(/(\/show\/([0-9]+))$/);
    return (
      <div>
        {/* Show FloatingAddButton for all paths,
          except /deadline/create */}
        { pathname.includes('/deadline/create')
          ? null : <FloatingAddButton /> }

        {/* Show FilterView for any path ending with /filter */}
        { pathname.match(/(\/filter)$/) ? <FilterView /> : null }

        {/* Show UserDetail for any path ending with /user-detail */}
        { pathname.match(/(\/user-detail)$/) ? <UserDetail /> : null }

        {/* Show deadline detail view for any path ending with: */}
        {/* /show/:id, where :id is a numeric value */}
        { detailMatch !== null && detailMatch.length == 3 ? (
          <CreationView cardId={detailMatch[2]} />
        ) : null }
      </div>
    );
  }

  renderProtected(user, location) {
    if (user.loggedIn !== true) {
      // Redirect to the /login path when not logged in
      return (
        <Redirect to={{
          pathname: '/login',
          state: { from: location }
        }} />
      );
    }

    return (
      <div>
        <Switch>
          <Route path="/calendar" component={CalendarView} />
          <Route path="/" component={GridView} />
        </Switch>
      </div>
    );
  }

  render() {
    const {cookies} = this.props;
    if (this.props.user.loggedIn === false && cookies.get('api_token')) {
      this.props.fetchUserDetail(cookies.get('api_token'));
      // @TODO: Hier een fancy loader ofzoiets
      return (
        <div>Loading stored user information..</div>
      );
    }

    const {pathname} = this.props.location;
    const {match} = this.props;
    return (
      <div>
        <div>
          <MenuBar />
          <Switch>
            {/* Main Content Component (select best match) */}
            <Route path="/register" component={RegisterView} />
            <Route path="/logout" component={LogoutView} />
            <Route path="/login" component={LoginView} />
            {this.renderProtected(this.props.user, this.props.location)}
          </Switch>
          {this.props.user.loggedIn && this.renderOverlays(pathname, match)}
        </div>
      </div>
    );
  }
}

function mapStateToProps({user}) {
  return { user, cookies: new Cookies() };
}

App = connect(mapStateToProps, {fetchUserDetail})(App); // react-redux
App = withRouter(App); // react-router-dom
export default App;
