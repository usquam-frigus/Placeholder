import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { attemptLogin } from '../../actions';

class LoginView extends Component {
  constructor(props) {
    super(props);
  }

  renderField(field) {
    const { meta } = field;
    const err = meta.touched && meta.error && (
      <div className="alert alert-danger" style={{
        paddingTop:5,
        paddingBottom:5
      }}>
       {meta.error}
      </div>
    );

    return (
      <div className="col-md-12 form-group">
        <label htmlFor={field.name}>{field.label}</label>
        <input name={field.name} placeholder={field.placeholder}
          className="form-control"
          id={field.name}
          {...field.input}
          type={field.type} />
        {err}
      </div>
    )
  }

  onSubmit(values) {
    this.props.attemptLogin(values);
  }

  renderError(err) {
    return (
      <div className="alert alert-danger" style={{
        paddingTop:5,
        paddingBottom:5
      }}>
        {err}
      </div>
    );
  }

  render() {
    if (this.props.user.loggedIn === true) {
      // Redirect to the default path ('/') when logged in
      return (
        <Redirect to={{
          pathname: '/',
          state: { from: this.props.location }
        }} />
      );
    }

    const {handleSubmit} = this.props;
    const {error} = this.props.user;
    return (
      <div>
        <div className="col-md-offset-4 col-md-4">
          <div className="panel panel-default">
            <header className="panel-heading">Inloggen</header>

            <div className="panel-body">
              <form role="login"
                onSubmit={handleSubmit(this.onSubmit.bind(this))}>
                {error ? this.renderError(error) : null}
                <Field label="E-mailadres" name="email"
                  placeholder="b.jansen@hotmail.com"
                  type="email"
                  component={this.renderField} />
                <Field label="Wachtwoord" name="password"
                  placeholder="Habbo123"
                  type="password"
                  component={this.renderField} />
                <div className="form-group">
                  <button type="submit"
                    className="btn btn-success btn-block">
                    Inloggen
                  </button>
                </div>
              </form>
              <p className="text-center">Of</p>

              <button className="btn btn-primary btn-block">
                Inloggen met Facebook
              </button>
              <button className="btn btn-danger btn-block">
                Inloggen met Google
              </button>

              <p className="text-center" style={{ marginTop: 15 }}>

              </p>
            </div>
            <div className="panel-footer">
              <Link to="/register">
                Registreer hier een account
              </Link>
              <Link className="pull-right" to="/forgotpassword">
                Wachtwoord vergeten?
              </Link>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps({user}) {
  return {user};
}

console.log("Todo: further expand the validation rules");
function validate(values) {
  const errors = {};
  if (!values.email) {
    errors.email = "Need to enter an email";
  }
  if (!values.password) {
    errors.password = "Need to enter a password";
  }
  // If errors is empty, the form is fine to submit
  return errors;
}

LoginView = reduxForm({
  validate,
  form: "UserLoginForm"
})(LoginView);

LoginView = connect(mapStateToProps, {attemptLogin})(LoginView);
export default LoginView;
