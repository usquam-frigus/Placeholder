import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';

import { updateFilter } from '../../actions';

import FilterLabels from './filter_labels';
import FilterMatchLabels from './filter_match_labels';
import FilterSort from './filter_sort';
import FilterColors from './filter_colors';
import FilterHeading from './filter_heading';

class FilterView extends Component {
  render() {
    const bgStyling = {
      position: 'fixed',
      top: 0,
      left: 0,
      width: '100%',
      height: '100%',
      zIndex: 50,
      background: 'rgba(34, 34, 34, 0.8)'
    }
    const popUpStyling = {
      zIndex: 100,
      position: 'fixed',
      top: 75,
      display: 'block',
      minWidth: 400,
      left: '50%',
      background: '#F8F8F8',
      padding: 0,
      transform: 'translateX(-50%)'
    };

    let {pathname} = this.props.location;
    pathname = pathname.replace(/(\/filter)$/, '');
    return (
      <div style={bgStyling}>
        <div className="container" style={popUpStyling}>
          <div className="panel panel-default filter-panel">
            <div className="panel-heading">
              <FilterHeading closePath={pathname} />
            </div>
            <div className="panel-body">
              <FilterLabels />
              <FilterMatchLabels style={{ marginBottom: 20 }} />
              <FilterColors style={{ marginBottom: 20 }} />
              <FilterSort />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

FilterView = withRouter(FilterView);
FilterView = connect(null, {updateFilter})(FilterView);
export default FilterView;
