import React, { Component } from 'react';
import { CirclePicker } from 'react-color';
import { connect } from 'react-redux';
import { updateFilter } from '../../actions';
import { MdClose } from 'react-icons/lib/md';

class FilterColors extends Component {
	constructor(props) {
		super(props);

		this.handleColorsChange = this.handleColorsChange.bind(this);
		let colors = [ "#fff" ];
		let filter = JSON.parse(localStorage.getItem('filter'));
		if (filter !== null) colors = filter.colors;
		this.state = { color: colors[0] };
	}

	handleColorsChange({hex}) {
		this.setState({color: hex});

		this.props.updateFilter({colors: [hex]});
	}

	clearColorFilter(e) {
		this.setState({color: ''});
		this.props.updateFilter({colors: []});
	}

  render() {
    return (
			<div className="row" style={{margin: 0, padding: 0}}>
				<div className="col-xs-2" style={this.props.style || {}}>
					<span>Kleur:</span>
				</div>
				<div className="col-xs-8" style={this.props.style || {}}>
					<CirclePicker color={this.state.color}
						onChange={this.handleColorsChange} />
				</div>
				<MdClose onClick={this.clearColorFilter.bind(this)}
					className="hover-darken col-xs-2"
					style={{
						fontSize: '2em',
						color: '#999',
						marginTop: 40,
						cursor: 'pointer'
					}} />
			</div>
    );
  }
}

export default connect(null, {updateFilter})(FilterColors);
