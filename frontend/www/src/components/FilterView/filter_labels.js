import React, { Component } from 'react';
import { connect } from 'react-redux';
import { updateFilter } from '../../actions';
import { MdLabel } from 'react-icons/lib/md';

class FilterLabel extends Component {
  constructor(props) {
    super(props);

    this.handleRemove = this.handleRemove.bind(this);

    let filter = JSON.parse(localStorage.getItem('filter'));
    let labels = [];
    if (filter !== null) labels = filter.labels;
    this.state = { labels, newLabel: '' };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.filter.labels !== undefined) {
      this.setState({labels: nextProps.filter.labels});
    }
  }

  handleSubmit(e) {
    e.preventDefault();

    let newLabel = this.state.newLabel.trim();
    if (newLabel === "") return;

    // add new label to component state
    // and clear the newLabel input text
    const newLabels = [...this.state.labels, newLabel];
    this.setState({labels: newLabels, newLabel: ""});

    // update app state (filter) with new label
    this.props.updateFilter({labels: newLabels});
  }

  handleRemove(e) {
    if (e.keyCode !== 8 || this.state.newLabel.trim() !== "") return;
    let newLabels = this.state.labels; newLabels.pop();
    this.setState({labels: newLabels});
    // update app state (filter) with removed label
    this.props.updateFilter({labels: newLabels});
  }

  render() {
    const labels = this.state.labels.map((l, i) => (
      <span style={{
        background: "#00BCD4", padding: 5, marginRight: 10, borderRadius: 5,
        verticalAlign: 'sub', display: 'inline-block'
      }} key={i}>
        {l}
      </span>
    ));

    return (
			<div className="filter-label">
				<div className="col-xs-2">
					<span>
            Labels:
          </span>
				</div>
        <div className="col-xs-10 tag-box" style={{width: '75%'}}>
          <MdLabel style={{ marginRight: 10 }} />
          {labels}
          <form onSubmit={this.handleSubmit.bind(this)} style={{ display: 'inline-block' }}>
            <input type="text" className="form-control"
              onChange={(e) => this.setState({newLabel: e.target.value})}
              onKeyDown={this.handleRemove}
              value={this.state.newLabel}
              style={{
                border: 'none', width: 'auto', minWidth: 75, boxShadow: 'none',
                display: 'inline-block', background: 'none', verticalAlign: 'sub',
                borderBottom: '2px dotted #999'
              }} />
          </form>
        </div>
			</div>
    );
  }
}

function mapStateToProps({filter}) {
	return {filter};
}

export default connect(mapStateToProps, {updateFilter})(FilterLabel);
