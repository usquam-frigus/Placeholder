import React, { Component } from 'react';
import { connect } from 'react-redux';
import { updateFilter } from '../../actions';
import {
  MdArrowDropDown, MdArrowDropUp
} from 'react-icons/lib/md';

class FilterSort extends Component {
  constructor(props) {
    super(props);

    this.handleSortChange = this.handleSortChange.bind(this);
    this.handleOrderChange = this.handleOrderChange.bind(this);

    let sortBy = { col: "startDate", order: "ASC" };
    let filter = JSON.parse(localStorage.getItem('filter'));
    if (filter !== null) sortBy = filter.sortBy;
    this.state = { sortBy };
  }

  handleSortChange(e) {
    this.setState({
      sortBy: { col: e.target.value, order: this.state.sortBy.order }
    });

    this.props.updateFilter({sortBy:
      { col: e.target.value, order: this.state.sortBy.order }
    });
  }

  handleOrderChange(e) {
    let flip = this.state.sortBy.order == "ASC" ? "DESC" : "ASC";
    this.setState({
      sortBy: { col: this.state.sortBy.col, order: flip }
    });

    this.props.updateFilter({sortBy:
      { col: this.state.sortBy.col, order: flip }
    });
  }

  renderOrderBtn(order) {
    const style = { fontSize: '2em' };
    if (order == "ASC") {
      return <MdArrowDropUp style={style} />;
    }
    return <MdArrowDropDown style={style} />;
  }

  render() {
    return (
      <div>
        <div className="col-xs-2">
          <span>
            Sortering:
          </span>
        </div>
        <div className="col-xs-10">
          <div className="form-group input-group col-sm-12">
            <select value={this.state.sortBy.col}
              onChange={this.handleSortChange}
              className="form-control">
              <option value="startDate">Datum</option>
              <option value="title">Titel</option>
              <option value="color">Kleur</option>
            </select>
            <div className="input-group-btn">
              <button onClick={this.handleOrderChange}
                style={{ padding: 2 }}
                className="btn btn-default">
                {this.renderOrderBtn(this.state.sortBy.order)}
              </button>
            </div>
          </div>
				</div>
      </div>
    );
  }
}

export default connect(null, { updateFilter })(FilterSort);
