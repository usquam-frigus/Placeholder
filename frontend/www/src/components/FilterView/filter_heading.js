import React from 'react';
import { Link } from 'react-router-dom';
import { MdClose } from 'react-icons/lib/md';

export default (props) => {
  return (
    <div>
      <span style={{ fontSize: '1.5em' }}>
        Filter deadlines
      </span>
      <div className="pull-right">
        <Link to={props.closePath}>
          <MdClose style={{ fontSize: '1.5em' }} />
        </Link>
      </div>
    </div>
  );
}
