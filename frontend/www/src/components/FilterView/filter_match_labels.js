import React, { Component } from 'react';
import { connect } from 'react-redux';
import { updateFilter } from '../../actions';

class FilterMatchLabels extends Component {
	constructor(props) {
		super(props);

		let filter = JSON.parse(localStorage.getItem('filter'));
		let match = "matchAny";
		if (filter !== null) match = filter.match;
		this.state = { match };
	}

	handleMatchChange(e) {
		this.setState({match: e.target.value});
		this.props.updateFilter({match: e.target.value});
	}

  render() {
		const radioMargins = { margin: 0, marginLeft: 10, marginRight: 10 };
		return (
	    <div className="col-xs-9 col-xs-offset-3"
				style={this.props.style || {}}>
		    <label style={{ marginRight: 10 }}>
					Match alles
					<input type="radio"
						style={radioMargins}
						onChange={this.handleMatchChange.bind(this)}
						checked={this.state.match === "matchAll"}
						name="match" value="matchAll" />
				</label>
				<label>
					Match één
					<input type="radio"
						style={radioMargins}
						onChange={this.handleMatchChange.bind(this)}
						checked={this.state.match === "matchAny"}
						name="match" value="matchAny" />
				</label>
	    </div>
		);
  }
}

export default connect(null, {updateFilter})(FilterMatchLabels);
