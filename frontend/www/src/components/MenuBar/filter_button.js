import React, { Component } from 'react';
import { MdFilterList } from 'react-icons/lib/md';
import { Link, withRouter, Route, Switch } from 'react-router-dom';

class FilterButton extends Component {
  openFilter(props) {
    // remove trailing '/'
    const basePath = props.location.pathname.replace(/\/{1}$/, '');
    return (
      <Link to={`${basePath}/filter`}
        style={{ color: '#555', marginRight: 20 }}>
        <MdFilterList style={{ fontSize: '2em', marginTop: 12 }} />
      </Link>
    );
  }

  closeFilter(props) {
    const closePath = props.location.pathname.replace(/\/filter/, '');
    return (
      <Link to={closePath}
        style={{ color: '#555', marginRight: 20 }}>
        <MdFilterList style={{ fontSize: '2em', marginTop: 12 }} />
      </Link>
    );
  }

  render() {
    const pathname = this.props.location.pathname;
    return (
      <div>
        {
          pathname.includes("filter")
          ? this.closeFilter(this.props)
          : this.openFilter(this.props)
        }
      </div>
    );
  }
}

export default withRouter(FilterButton);
