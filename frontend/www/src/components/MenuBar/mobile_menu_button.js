import React from 'react';

export default (props) => {
  return (
    <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target={props.target} aria-expanded="false">
      <span className="sr-only">Toggle navigation</span>
      <span className="icon-bar"></span>
      <span className="icon-bar"></span>
      <span className="icon-bar"></span>
    </button>
  );
}
