import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchDeadlines } from '../../actions/index';

class SearchBar extends Component {
  constructor(props) {
    super(props);

    this.state = { term: '' };

    this.onInputChange = this.onInputChange.bind(this);
    this.onFormSubmit = this.onFormSubmit.bind(this);
  }

  onInputChange(event) {
    this.setState({ term: event.target.value });
  }

  onFormSubmit(event) {
    event.preventDefault();
    this.props.fetchDeadlines(this.props.filter, this.state.term);
  }

  render() {
    return (
      <div className={this.props.className || null}>
        <form role="search"
          className="navbar-form"
          onSubmit={this.onFormSubmit}>
          <div className="form-group input-group col-sm-12">
            <input type="text"
              onChange={this.onInputChange}
              value={this.state.term}
              className="form-control"
              placeholder="Search" />
            <div className="input-group-btn">
              <button type="submit" className="btn btn-default">
                Go!
              </button>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

function mapStateToProps({filter}) {
  return {filter};
}

export default connect(mapStateToProps, {fetchDeadlines})(SearchBar);
