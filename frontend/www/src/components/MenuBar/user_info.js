import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import {fetchUserDetail} from '../../actions';

class UserInfo extends Component {
  renderUser() {
    let {pathname} = this.props.location;
    pathname = pathname.replace(/(\/user-detail)$/, '');
    pathname = pathname.replace(/\/+$/, ''); // remove trailing /
    return (
      <span>
        Signed in as&nbsp;
        <Link to={`${pathname}/user-detail`}>
          {this.props.user.user.name || ""}
        </Link>
      </span>
    );
  }

  renderGuest() {
    return (
      <Link to="/login">
        Sign in
      </Link>
    );
  }

  render() {
    return (
      <span className={this.props.className || null}>
        { this.props.user.loggedIn ? this.renderUser.bind(this)()
          : this.renderGuest() }
      </span>
    );
  }
}


function mapStateToProps({user}) {
  return {user: user};
}

export default withRouter(
  connect(mapStateToProps, {fetchUserDetail})(UserInfo)
);
