// @DEPRECATED COMPONENT

import React, { Component } from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  changeCalendarType,
  calendarNext,
  calendarPrev
} from '../../actions/index';

class CalendarSelect extends Component {
  constructor(props) {
    super(props);

    this.state = { selected: 0 };

    // bind the onInputChange to the current object context
    // this way we can prevent it from being called in a mystery context
    // (this.setState may not exist in mystery context)
    this.selectOnChange = this.selectOnChange.bind(this);
    this.onFormSubmit = this.onFormSubmit.bind(this);
  }

  onFormSubmit(event) {
    event.preventDefault();
  }

  selectOnChange(event) {
    this.setState({selected: event.target.value});
    if (event.target.value !== '0') {
      this.props.changeCalendarType(event.target.value);
    }
  }

  render() {
    return (
      <div className={this.props.className || null}>
        <form role="select"
          className="navbar-form"
          onSubmit={this.onFormSubmit}>
          <div className="form-group input-group col-sm-12">
            <div className="input-group-btn">
              <button type="button"
                onClick={
                  () => this.props.calendarPrev(this.props.calendarType)
                }
                className="btn btn-default calendar-nav-btn">
                <span className="glyphicon glyphicon-chevron-left"></span>
              </button>
            </div>
            <select value={this.state.selected}
              onChange={this.selectOnChange}
              className="form-control calendar-type-select">
              <option value={0}>Mei 2017</option>
              <option value="month">Month</option>
              <option value="week">Week</option>
              <option value="day">Day</option>
              <option value="agenda">Agenda</option>
            </select>
            <div className="input-group-btn">
              <button type="button"
                onClick={
                  () => this.props.calendarNext(this.props.calendarType)
                }
                className="btn btn-default calendar-nav-btn">
                <span className="glyphicon glyphicon-chevron-right"></span>
              </button>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

function mapStateToProps({calendarType}) {
  return {calendarType: calendarType};
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    changeCalendarType,
    calendarNext,
    calendarPrev
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(CalendarSelect);
