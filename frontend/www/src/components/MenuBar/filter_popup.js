import React, { Component } from 'react';
import MdClear from 'react-icons/lib/md/clear';
import { TwitterPicker } from 'react-color';
import { Calendar } from 'react-date-picker';
// import 'react-date-picker/index.css';

export default class Filter extends Component {
    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-offset-2 col-md-8">
                        <div className="popup flex-child panel panel-default creation-panel">
                            <div className="panel-body">
                                <h3 className="panel-title pull-left">Filter based on</h3>
                                <button type="button" className="btn btn-default btn-circle pull-right btn-extra">
                                    <MdClear />
                                </button>
                                <div className="clearfix"></div>
                            </div>
                            <div className="panel-body">
                                <div className="row filter-label">
                                    <div className="col-xs-3">
                                         <p>Label:</p>
                                    </div>
                                    <div className="col-xs-9">
                                        <span className="glyphicon glyphicon-tag"></span>
                                        <span className="btn label label-default">2.1 Computer Systems</span>
                                        <span className="btn label label-warning">Communicatie</span>
                                    </div>
                                    <div className="col-xs-9">
                                        <input type="radio" name="match" value="matchAll">Match all</input>
                                        <input type="radio" name="match" value="matchAny">Match any</input>
                                    </div>
                                </div>
                                <div className="row filter-label">
                                    <div className="col-xs-3">
                                         <p>Date:</p>
                                    </div>
                                    <div className="col-xs-9">
                                        {/*<Calendar />*/}
                                    </div>
                                </div>
                                <div className="row filter-label">
                                    <div className="col-xs-3 tag-box">
                                        <p>Title:</p>
                                    </div>
                                    <div className="col-xs-9 tag-box">
                                        <select className="selectpicker">
                                          {/*<optgroup label="Title ">*/}
                                            <option>Werkplan</option>
                                            <option>Reflectieverslag Communicatie</option>
                                            <option>OOP tentamen</option>
                                          {/*</optgroup>*/}
                                          {/*<optgroup label="Camping">*/}
                                            <option>Databases week 1</option>
                                          {/*</optgroup>*/}
                                        </select>
                                        </div>
                                </div>
                                <div className="row filter-label">
                                    <div className="col-xs-3 tag-box">
                                    	<p>Color:</p>
                                    </div>
                                    <div className="col-xs-9 tag-box">
                                        <TwitterPicker />
                                    </div>
                                </div>
                            </div>
                            <div className="panel-footer">
                                <div className="row">
                                    <div className="col-xs-12">
                                        <button type="button" className="btn btn-primary pull-right">save</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
