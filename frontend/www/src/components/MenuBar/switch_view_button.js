import React, { Component } from 'react';
import { MdToday, MdViewModule } from 'react-icons/lib/md';
import { Link, Route, Switch } from 'react-router-dom';
import { connect } from 'react-redux';

export default class SwitchViewButton extends Component {
  calendarLink() {
    return (
      <Link to="/calendar" style={{ color: '#555' }}>
        <MdToday style={{ fontSize: '2em', marginTop: 12 }} />
      </Link>
    );
  }

  gridLink() {
    return (
      <Link to="/" style={{ color: '#555' }}>
        <MdViewModule style={{ fontSize: '2em', marginTop: 12 }} />
      </Link>
    );
  }

  render() {
    return (
      <div>
        <Switch>
          <Route path="/calendar" component={this.gridLink} />
          <Route path="/" component={this.calendarLink} />
        </Switch>
      </div>
    );
  }
}
