import React, {Component} from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import MenuBrand from './menu_brand';
import SearchBar from './search_bar';
import UserInfo from './user_info';
import FilterButton from './filter_button';
import SwitchViewButton from './switch_view_button';
import MobileMenuButton from './mobile_menu_button';

class MenuBar extends Component {
  render() {
    const loggedIn = this.props.user.loggedIn;
    return (
      <nav className="navbar navbar-default navbar-static-top">
        <div className="container-fluid">
          <div className="navbar-header">
            {/* Brand and menu toggle get grouped for better mobile display */}
            <MenuBrand />
            <MobileMenuButton target="#mainMenu" />
          </div>

          {/* Collect the nav links, forms, and other content for toggling */}
          <div className="collapse navbar-collapse" id="mainMenu">
            { loggedIn && (
              <div>
                <div className="col-sm-6 col-md-7 col-lg-8">
                  <SearchBar />
                </div>
                <ul className="nav navbar-nav">
                  <li>
                    <FilterButton />
                  </li>
                  <li>
                    <SwitchViewButton />
                  </li>
                </ul>
              </div>
            ) }
            <UserInfo className="navbar-text navbar-right" />
          </div> {/* .navbar-collapse */}
        </div>
      </nav>
    );
  }
}

function mapStateToProps({user}) {
  return {user};
}

export default withRouter(connect(mapStateToProps)(MenuBar));
