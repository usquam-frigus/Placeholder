import React, { Component } from 'react';
import { connect } from 'react-redux';
import { logoutUser } from '../../actions';
import { Link } from 'react-router-dom';

class LogoutView extends Component {
  renderLoggedOf() {
    return (
      <div>
        <p>You have succesfully logged off.</p>
        <p>Click <Link to="/login">here</Link> to login or register.</p>
      </div>
    );
  }

  componentDidMount() {
    if (this.props.user.loggedIn === true) {
      this.props.logoutUser();
    }
  }

  render() {
    return (
      <div>
        {this.props.user.loggedIn === true
          ? "Logging off.."
          : this.renderLoggedOf()}
      </div>
    );
  }
}

function mapStateToProps({user}) {
  return {user};
}

export default connect(mapStateToProps, {logoutUser})(LogoutView);
