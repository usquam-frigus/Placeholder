import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

import userReducer from './reducer_user';
import deadlineReducer from './reducer_deadlines';
import filterReducer from './reducer_filter';

const rootReducer = combineReducers({
    user: userReducer,
    deadlines: deadlineReducer,
    filter: filterReducer,
    form: formReducer
});

export default rootReducer;
