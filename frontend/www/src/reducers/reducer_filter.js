import {
  UPDATE_FILTER
} from '../actions/types';
import update from 'immutability-helper';

let defaultState = {
  labels: [],
  colors: [],
  sortBy: {col: "startDate", order: "ASC"},
  match: "matchAny"
};
let filter = JSON.parse(localStorage.getItem('filter'));
if (filter !== null) {
  defaultState = filter;
}

export default (state = defaultState, action) => {
  switch (action.type) {
    case UPDATE_FILTER:
      let newFilter = update(state, { $merge: action.payload });
      localStorage.setItem('filter', JSON.stringify(newFilter));
      return newFilter;
    break;
  }

  return state;
}
