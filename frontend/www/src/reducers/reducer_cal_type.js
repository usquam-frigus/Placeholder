import {
  CHANGE_CAL_TYPE
} from '../actions/types';

const defaultState = "month";

export default (state = defaultState, action) => {
  return {
    CHANGE_CAL_TYPE: action.payload
  }[action.type] || state;
}
