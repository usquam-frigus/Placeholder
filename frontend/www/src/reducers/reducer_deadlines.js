import moment from 'moment';
import _ from 'lodash';
import update from 'immutability-helper';
import {
  FETCH_DEADLINES,
  NEW_DEADLINES,
  UPDATE_DEADLINES,
  DELETE_DEADLINE,
  CREATE_DEADLINE,
  UPDATE_DEADLINE_LABELS,
  UPDATE_DEADLINE,
  DELETE_OPTION,
  ADD_OPTION,
  UPDATE_OPTION
} from '../actions/types';

export default (state = {}, action) => {
  switch (action.type) {
      case FETCH_DEADLINES:
        let deadlines = _.mapKeys(action.payload.data, 'id');
        deadlines.sortedArr = action.payload.data;
        return deadlines;
      break;
      case UPDATE_DEADLINES:
        return action.payload.deadlines;
      break;
      case UPDATE_DEADLINE:
        if (action.payload === null) return state;
        let {deadline} = action.payload.data;
        let newState1 = state;
        newState1.sortedArr = _.map(state.sortedArr, (d) => {
          if (d.id === deadline.id) {
            return Object.assign(d, deadline);
          }
          return d;
        });
        return update(newState1, {
          [deadline.id]: {$merge: deadline}
        });
      break;
      case CREATE_DEADLINE:
        return update(state, {
          [action.payload.data.id]: {$set: action.payload.data}
        });
      break;
      case DELETE_DEADLINE:
        if (action.payload === null) return state;
        state.sortedArr = state.sortedArr
          .filter((d) => d.id !== action.payload.data.id);
        return update(state, {
          $unset: [action.payload.data.id]
        });
      break;
      case ADD_OPTION:
        let {option} = action.payload.data;
        const newState = update(state, {
          [option.deadline_id]: {options: {$push: [option]}}
        });
        return newState;
      break;
      case UPDATE_OPTION:
        let updatedOption = action.payload.data.option;
        let updatedOptions = state[updatedOption.deadline_id].options.map(o => {
          return o.id !== updatedOption.id ? o : updatedOption;
        });
        return update(state, {
          [updatedOption.deadline_id]: {options: {$set: updatedOptions}}
        });
      break;
      case UPDATE_DEADLINE_LABELS:
        return update(state, {
          [action.payload.data.id]: {labels: {$set: action.payload.data.labels}}
        })
      break;
      case DELETE_OPTION:
        let delOptId = parseInt(action.payload.data.id);
        let d_id = parseInt(action.payload.data.deadline_id);
        let newOpts = state[d_id].options.filter(o => {
          return o.id !== delOptId;
        });
        return update(state, {
          [d_id]: {options: {$set: newOpts}}
        });
      break;
  }

  return state;
}
