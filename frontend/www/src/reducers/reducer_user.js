import {
  FETCH_USER,
  ATTEMPT_LOGIN,
  REGISTER_USER,
  LOGOUT_USER
} from '../actions/types';
import Cookies from 'universal-cookie';
const cookies = new Cookies();

const defaultState = {
  loggedIn: false
};

function updateAuthCookie({api_token}, {status}) {
  if (status === 200 && api_token !== cookies.get('api_token')) {
    cookies.set('api_token', api_token, {
      path: "/",
      maxAge: 50000
    });
  }
}

export default function(state = defaultState, action) {
  switch (action.type) {
    case FETCH_USER:
      return {
        loggedIn: action.payload.status === 200 ? true : false,
        user: action.payload.data
      };
    break;
    case ATTEMPT_LOGIN:
      if (action.payload.status === 200) {
        updateAuthCookie(action.payload.data.user, action.payload);
      }
      return action.payload.data;
    break;
    case REGISTER_USER:
      if (action.payload.status === 400) {
        let error = false;
        if (action.payload.data.code === "ER_DUP_ENTRY") {
          error = "E-mailadres is al in gebruik.";
        }
        return {
          loggedIn: false,
          error: error !== undefined ? error : "Onbekende fout"
        }
      }
      updateAuthCookie(action.payload.data, action.payload);
      return {
        loggedIn: action.payload.status === 200 ? true : false,
        user: action.payload.data
      };
    break;
    case LOGOUT_USER:
      if (action.payload.status == "success") {
        return { loggedIn: false };
      }
    break;
  }
  return state;
}
