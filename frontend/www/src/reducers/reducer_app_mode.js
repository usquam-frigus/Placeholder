import {
  CHANGE_APP_MODE
} from '../actions/types';

const defaultState = "grid";

export default (state = defaultState, action) => {
  return {
    CHANGE_APP_MODE: action.payload
  }[action.type] || state;
}
