import router from './api/routes';
import bodyParser from 'body-parser';
import passport from 'passport';
import { Strategy as BearerStrategy } from 'passport-http-bearer';
import { User } from './api/models/model';

passport.use(new BearerStrategy(
  (token, cb) => {
    new User({api_token: token}).fetch().then((user) => {
      if (user === null) return cb(null, false);
      return cb(null, user, { user: user });
    });
  }
));

// start the express webserver
const express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;
app.listen(port);

// parse request body (json)
app.use(bodyParser.json());

// enable cross origin requests (only for localhost right now)
import cors from 'cors';
app.use(cors({
  origin: 'http://localhost:8080',
  optionsSuccessStatus: 200
}));

// bootstrap the api routes within ./api/routes
router(app);

console.log("Placeholder backend server started on: " + port);
