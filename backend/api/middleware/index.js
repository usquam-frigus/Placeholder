export const allowCrossDomain = function(req, res, next) {
  // allow dev environments
  // @TODO: allow production environment CORS
  res.header('Access-Control-Allow-Origin', 'localhost:8080');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  next();
}
