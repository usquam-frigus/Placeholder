import {knex} from '../../db/bookshelf';
import bookshelf from '../../db/bookshelf';
import { User, Deadline, Option, Label } from '../models/model';
import bcrypt from 'bcrypt';
import Checkit from 'checkit';
import moment from 'moment';
import _ from 'lodash';
import cascadeDelete from 'bookshelf-cascade-delete';

bookshelf.plugin(cascadeDelete);
const saltRounds = 10;

export const UsersController = {
  index(req, res) {
    User.fetchAll({
      columns: ['id', 'name', 'email']
    }).then((models) => res.json(models));
  },

  create(req, res) {
    res.send("UsersController create action reached");
  },

  show(req, res) {
    new User({ id: req.params.id })
    .fetch({
      columns: ['id', 'name', 'email']
    }).then((model) => {
      if (model === null) {
        return res.status(400).json({
          error: "user cannot be found"
        });
      }
      return res.json(model);
    });
  },

  update(req, res) {
    res.send("UsersController update action reached");
  },

  delete(req, res) {
    res.send("UsersController delete action reached");
  },

  getDetail(req, res) {
    res.json(req.user.omit("password"));
  }
};

export const AuthController = {
  login(req, res) {
    const { email, password } = req.body;
    new User({ email: email })
      .fetch()
      .then((user) => {
        if (user === null) return res.status(400).json({
          loggedIn: false,
          error: "E-mailadres not found"
        });
        User.verifyPassword(password, user)
          .then((result) => {
            if (result === true) {
              return res.json({
                loggedIn: true,
                user: user.omit("password")
              });
            }
            return res.status(400).json({
              loggedIn: false,
              error: "Incorrect password"
            });
          })
      });
  },

  register(req, res) {
    // Validate postData
    new Checkit({
      name: 'required',
      email: ['required', 'email'],
      password: 'required'
    }).run(req.body).then((valid) => {
      // Create new User model
      new User({
        name: valid.name,
        email: valid.email,
        password: bcrypt.hashSync(valid.password, saltRounds),
        api_token: bcrypt.hashSync(
          String(new Date().getTime()/1000|0), 1
        )
      }).save()
      .then((user) => {
        // Return User model with id (omit password)
        res.json(user.omit('password'));
      }).catch((err) => {
        res.status(400).json(err);
      });
    }).catch((err) => {
      res.status(400).json(err.toJSON());
    });

  },

  forgotPassword(req, res) {
    res.send("forgotPassword called");
  },

  resetPassword(req, res) {
    res.send("resetPassword called");
  }
};

export const DeadlineController = {
  index(req, res) {
    const {filter} = req.body;
    const {sortBy} = filter;
    let search = req.body.search !== null ? req.body.search : "";
    new User({ id: req.authInfo.user.id })
      .fetch({withRelated: [
        { deadlines: (qb) => {
          qb.where('deadlines.title', 'LIKE', `%${search}%`)
            // .orWhere('deadlines.text', 'LIKE', `%${search}%`)
            .orderBy(sortBy.col, sortBy.order); // @TODO: does not work?
        }}, 'deadlines.labels', 'deadlines.options'
      ]})
      .then((user) => {
        res.json(Deadline.filterDeadlines(user.related('deadlines'), filter));
      })
      .catch((err) => {console.log(err);res.status(400).json({error: err})});
  },

  create(req, res) {
    if (req.body.user_id === undefined) req.body.user_id = req.authInfo.user.id;
    if (req.body.startDate !== undefined) {
      req.body.startDate = moment(req.body.startDate)
        .format("YYYY-MM-DD HH:mm:ss");
    }
    Deadline.validator.run(req.body).then((valid) => {
      Deadline.create(valid).then(deadline => {
        res.json(deadline);
      });
    }).catch((err) => {
      console.log(err);
      res.status(400).json({error: err});
    });
  },

  show(req, res) {
    res.json({msg: "DeadlineController show action reached"});
  },

  update(req, res) {
    // @TODO: proper validation should be added
    if (req.body.startDate !== undefined) {
      req.body.startDate = moment(req.body.startDate)
        .format("YYYY-MM-DD HH:mm:ss");
    }
    new Deadline(req.body).save()
      .then((d) => {
        res.json({deadline: d});
      })
      .catch((err) => {
        res.status(400).json({error: err});
      })
  },

  addOption(req, res) {
    // @TODO: proper validation should be added
    new Option(req.body).save()
      .then((option) => {
        res.json({
          option: Object.assign(option, {deadline_id: req.body.deadline_id})
        });
      })
      .catch((err) => {
        res.status(400).json({error: err});
      });
  },

  deleteOption(req, res) {
    new Option(req.params).destroy()
      .then((o) => {
        res.json(req.params);
      })
      .catch((err) => {
        console.log(err);
        res.status(400).json({error: err});
      });
  },

  updateOption(req, res) {
    // @TODO: proper validation should be added
    // @TODO: should be refactored and put inside own controller
    new Option(req.body).save()
      .then((option) => {
        res.json({
          option: Object.assign(option, {deadline_id: req.body.deadline_id})
        });
      })
      .catch((err) => {
        res.status(400).json({error: err});
      });
  },

  updateLabels(req, res) {
    if (req.body.startDate !== undefined) {
      req.body.startDate = moment(req.body.startDate)
        .format("YYYY-MM-DD HH:mm:ss");
    }
    // @TODO: proper validation should be added
    // @TODO: should be refactored and put inside own controller
    Label.removeOldLabelLinks(req.body.id);
    Label.createLabelsForDeadline(req.body.labels, req.body.id);
    new Deadline({id: req.body.id}).fetch({withRelated: ['labels']})
      .then((deadline) => {
        res.json(deadline);
      })
      .catch((err) => {
        res.status(400).json({error: err});
      });
  },

  delete(req, res) {
    // @TODO: proper validation should be added
    // @TODO: current user should be checked
    knex.raw(
      `delete from deadlines_labels where deadline_id=${req.params.id}`
    ).then((resp) => {
      knex.raw(
        `delete from options where deadline_id=${req.params.id}`
      ).then((resp) => {
        Deadline.forge(req.params).destroy().then((resp) => {
            res.json(req.params);
          });
      });
    });
  },
}
