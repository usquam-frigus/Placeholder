import {
  UsersController,
  AuthController,
  DeadlineController
} from '../controllers/controller';
import passport from 'passport'

export default (app) => {

  // home (index) route: 'api.placeholder.com/'
  app.get('/', (req, res) => {
    res.send("Hello world from the home (index) route!");
  });

  // users routes (mainly for testing or an admin panel of sorts)
  app.get(
    '/users',
    passport.authenticate('bearer', {session: false}),
    UsersController.index
  );
  app.post(
    '/users',
    passport.authenticate('bearer', {session: false}),
    UsersController.create
  );
  app.get(
    '/users/:id',
    passport.authenticate('bearer', {session: false}),
    UsersController.show
  );
  app.put(
    '/users/:id',
    passport.authenticate('bearer', {session: false}),
    UsersController.update
  );
  app.delete(
    '/users/:id',
    passport.authenticate('bearer', {session: false}),
    UsersController.delete
  );
  app.get(
    '/user-detail',
    passport.authenticate('bearer', {session: false}),
    UsersController.getDetail
  );

  // authentication routes
  app.post('/login', AuthController.login);
  app.post('/register', AuthController.register);
  app.get('/forgotpassword', AuthController.forgotPassword);
  app.post('/resetpassword', AuthController.resetPassword);

  // deadlines routes
  app.post(
    '/deadlines',
    passport.authenticate('bearer', {session: false}),
    DeadlineController.index
  );
  app.post(
    '/deadlines-new',
    passport.authenticate('bearer', {session: false}),
    DeadlineController.create
  );
  app.delete(
    '/deadlines/:id',
    passport.authenticate('bearer', {session: false}),
    DeadlineController.delete
  );
  app.put(
    '/deadlines/:id',
    passport.authenticate('bearer', {session: false}),
    DeadlineController.update
  );
  app.post(
    '/options',
    passport.authenticate('bearer', {session: false}),
    DeadlineController.addOption
  );
  app.put(
    '/options/:id',
    passport.authenticate('bearer', {session: false}),
    DeadlineController.updateOption
  );
  app.delete(
    '/options/:id/:deadline_id',
    passport.authenticate('bearer', {session: false}),
    DeadlineController.deleteOption
  );
  app.put(
    '/deadline-labels/:id',
    passport.authenticate('bearer', {session: false}),
    DeadlineController.updateLabels
  );

}
