import bookshelf from '../../db/bookshelf';
import _ from 'lodash';
import bcrypt from 'bcrypt';
import Checkit from 'checkit';
import Promise from 'bluebird';

export const User = bookshelf.Model.extend({
  tableName: 'users',
  hasTimeStamps: true,
  tests: function() { return this.hasMany(Test); },
  deadlines: function() { return this.hasMany(Deadline); }
}, {
  verifyPassword: (password, model) => {
    if (!model || !password) {
      throw new Error('Password and a user model are both required');
    }
    return bcrypt.compare(password, model.get('password'));
  }
});

export const Test = bookshelf.Model.extend({
  tableName: 'tests',
  user: function() { return this.belongsTo(User); }
});

export const Option = bookshelf.Model.extend({
  hasTimeStamps: true,
  tableName: 'options',
  deadline: function() { return this.belongsTo(Deadline); }
}, {
  validator: new Checkit({
    text: 'required',
    isChecked: ['required', 'boolean']
  }),
  createOptions: (array, deadlineId) => {
    array.map(option => {
      option.deadline_id = deadlineId;
      new Option(option).save();
    });
  }
});

export const Label = bookshelf.Model.extend({
  hasTimeStamps: true,
  tableName: 'labels',
  deadlines: function() { return this.belongsToMany(Deadline); }
}, {
  validator: new Checkit({
    name: 'required',
    color: 'required'
  }),
  removeOldLabelLinks: (deadlineId) => {
    DeadlineLabel.query('where', 'deadline_id', '=', deadlineId)
      .fetchAll()
      .then((labels) => {
        return labels.map(l => {
          return l.destroy();
        });
      })
      .catch((err) => {
        return err;
      })
  },
  createLabelsForDeadline: (array, deadlineId) => {
    array.forEach(l => {
      new Label({name: l.name}).fetch().then(label => {
        return new Promise((resolve, reject) => {
          if (label === null) {
            delete l.id;
            new Label(l).save().then(newLabel => {
              resolve(newLabel.id);
            });
          } else {
            resolve(label.id);
          }
        });
      }).then((id) => {
        // and create their links (deadlines_labels)
        new DeadlineLabel({deadline_id: deadlineId, label_id: id}).save();
      });
    });
  }
});

export const DeadlineLabel = bookshelf.Model.extend({
  tableName: 'deadlines_labels',
  label() {
    return this.belongsTo(User, 'user_id');
  },
  deadline() {
    return this.belongsTo(Deadline, 'deadline_id');
  }
}, {
});

export const Deadline = bookshelf.Model.extend({
  hasTimeStamps: true,
  tableName: 'deadlines',
  user: function() { return this.belongsTo(User); },

  // Many to Many relationship
  labels: function() {
    return this.belongsToMany(Label, 'deadlines_labels');
  },
  options: function() { return this.hasMany(Option); }
}, {
  dependents: ['labels', 'options'],

  validator: new Checkit({
    title: 'required',
    startDate: 'required',
    color: 'required',
    user_id: 'required',
    labels: {
      // @TODO: for some reason errors are ignored
      rule: (labels, context) => Promise.map(labels, l => {
        Label.validator.run(l, context)
          .catch(err => { throw new Error(err); })
      })
    },
    options: {
      // @TODO: for some reason errors are ignored
      rule: (options, context) => Promise.map(options, o => {
        Option.validator.run(o, context)
          .catch(err => { throw new Error(err); })
      })
    }
  }),

  create: (validData) => {
    const {labels, options} = validData;
    delete validData.labels;
    delete validData.options;

    return new Deadline(validData).save().then(deadline => {
      return new Promise((resolve, reject) => {
        // create options
        Option.createOptions(options, deadline.id);

        // create labels (if needed)
        Label.createLabelsForDeadline(labels, deadline.id);

        resolve(deadline);
      });
    });
  },

  filterByLabels(deadlines, filter) {
    return _.filter(deadlines.toJSON(), (deadline) => {
      let labels = _.mapKeys(deadline.labels, 'name');
      if (filter.match === "matchAll") {
        labels = _.filter(labels, ({name}) => {
          return filter.labels.includes(name);
        });
        // console.log(labels.length, filter.labels.length);
        return labels.length === filter.labels.length;
      }
      // find the first object that is in the filter.labels array
      return _.find(labels, ({name}) => filter.labels.includes(name))
        !== undefined;
    });
  },

  filterByColor(deadlines, {colors}, isFiltered) {
    colors = _.map(colors, (c) => c.toLowerCase());

    if (typeof deadlines === 'object' && deadlines.length === 0) return [];
    if (!isFiltered) deadlines = deadlines.toJSON();

    return _.filter(deadlines, ({color}) => {
      return colors.includes(color);
    });
  },

  filterDeadlines: (deadlines, filter) => {
    let isFiltered = false;
    if (filter.labels !== null && filter.labels.length > 0) {
      deadlines = Deadline.filterByLabels(deadlines, filter);
      isFiltered = true;
    }
    if (filter.colors !== null && filter.colors.length > 0) {
      deadlines = Deadline.filterByColor(deadlines, filter, isFiltered);
    }
    return deadlines;
  }

});
