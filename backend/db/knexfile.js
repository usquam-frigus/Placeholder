// Update with your config settings.
require("babel-register")

var config = require('./dbconfig').connection;
// console.log(config);

module.exports = {
  development: {
    client: 'mysql',
    connection: {
      database: config.database,
      user:     config.user,
      password: config.password
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations'
    }
  }
};
