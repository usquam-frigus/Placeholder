import config from './dbconfig';
const knex = require('knex')(config);
const bookshelf = require('bookshelf')(knex);

export default bookshelf;
export {knex};
