import faker from 'faker';

exports.seed = function(knex, Promise) {
  console.log("3 | Seeding options..");

  // Generate 5,000 options
  let entries = [];
  for (let i = 0; i < 5000; i++) {
    entries[i] = {
      id: i + 1, // to solve array index starting at 0
      text: faker.name.jobDescriptor(),
      isChecked: faker.random.boolean(),
      deadline_id: Math.floor(Math.random() * 5000) + 1 // random id 1-5k
    };
  }

  return knex('options').insert(entries);
};
