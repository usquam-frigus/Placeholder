import faker from 'faker';

exports.seed = function(knex, Promise) {
  console.log("5 | Seeding deadlines_labels MtM table..");

  // Generate 5,000 deadlines_labels relations
  let entries = [];
  for (let i = 0; i < 5000; i++) {
    entries[i] = {
      id: i + 1, // to solve array index starting at 0
      deadline_id: Math.floor(Math.random() * 5000) + 1, // random id 1-5k
      label_id: Math.floor(Math.random() * 100) + 1 // random id 1-100
    };
  }

  return knex('deadlines_labels').insert(entries);
};
