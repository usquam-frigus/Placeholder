import faker from 'faker';

exports.seed = function(knex, Promise) {
  console.log("4 | Seeding labels..");

  // Generate 100 labels
  let entries = [];
  for (let i = 0; i < 100; i++) {
    entries[i] = {
      id: i + 1, // to solve array index starting at 0
      name: faker.address.streetName(),
      color: faker.internet.color(),
    };
  }

  return knex('labels').insert(entries);
};
