import faker from 'faker';
import bcrypt from 'bcrypt';

const saltRounds = 1; // note: number is just for development 

exports.seed = function(knex, Promise) {
  console.log("1 | Seeding users..");

  // Generate 1000 users
  let entries = [];
  for (let i = 0; i < 1000; i++) {
    entries[i] = {
      id: i + 1, // to solve array index starting at 0
      name: faker.name.findName(),
      email: faker.internet.email(),
      password: bcrypt.hashSync('habbo123', saltRounds),
      api_token: bcrypt.hashSync(
        String(new Date().getTime()/1000|0),
        1
      )
    };
  }
  return knex('users').insert(entries);
};
