import faker from 'faker';
import moment from 'moment';

exports.seed = function(knex, Promise) {
  console.log("2 | Seeding deadlines..");
  let colors = ["#F44336","#FFEB3B","#03A9F4","#00BCD4","#3F51B5","#F5F5F5"];
  colors = colors.map(c => c.toLowerCase());

  // Generate 5000 deadlines
  let entries = [];
  for (let i = 0; i < 5000; i++) {
    let colorI = Math.floor(Math.random() * colors.length-1);
    entries[i] = {
      id: i + 1, // to solve array index starting at 0
      title: faker.name.jobArea(),
      startDate: faker.date.between(
        moment().toDate(),
        moment().add('1', 'month')
      ),
      text: faker.lorem.paragraphs(3),
      color: colors[colorI],
      user_id: Math.floor(Math.random() * 1000) + 1 // random uid 1-1k
    };
  }

  return knex('deadlines').insert(entries);
};
