exports.seed = function(knex, Promise) {
  console.log("0 | Clearing tables..");
  return knex('tests').del() // tests is a dummy table, just ignore it
  .then(() => knex('deadlines_labels').del())
  .then(() => knex('labels').del())
  .then(() => knex('options').del())
  .then(() => knex('deadlines').del())
  .then(() => knex('users').del())
}
