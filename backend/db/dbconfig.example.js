var config = {
  client: 'mysql',
  connection: {
    host: '127.0.0.1',
    user: 'placeholder',
    password: 'secret',
    database: 'placeholder',
    charset: 'utf8'
  }
};

module.exports = config;
