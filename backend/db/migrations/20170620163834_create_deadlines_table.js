
exports.up = function(knex, Promise) {
  return knex.schema.createTable('deadlines', (table) => { // create deadlines
    table.increments('id').primary();
    table.string('title');
    table.dateTime('startDate'); // endDate is altijd een uur later
    table.text('text');
    table.text('color'); // hex color code
    table.integer('user_id').unsigned().references('users.id');
    table.timestamps();
  }).createTable('options', function(table) { // create options
    // One deadline can have 0..N options (checkboxes)
    table.increments('id').primary();
    table.string('text');
    table.boolean('isChecked');
    table.integer('deadline_id').unsigned().references('deadlines.id');
    table.timestamps();
  }).createTable('labels', function(table) { // create labels
    table.increments('id').primary();
    table.string('name').unique();
    table.string('color'); // hex color code
    table.timestamps();
  }).createTable('deadlines_labels', function(table) { // create deadlines_labels
    // One deadline can have 0..N labels (Many-to-many)
    // One label can have 0..N deadlines (Many-to-many)
    table.increments('id').primary();
    table.integer('deadline_id').unsigned().references('deadlines.id');
    table.integer('label_id').unsigned().references('labels.id');
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.table('deadlines_labels', function(table) {
      table.dropForeign('label_id');
      table.dropForeign('deadline_id');
    }).table('options', function(table) {
      table.dropForeign('deadline_id');
    }).table('deadlines', function(table) {
      table.dropForeign('user_id');
    })
    .dropTable('deadlines_labels')
    .dropTable('labels')
    .dropTable('options')
    .dropTable('deadlines');
};
