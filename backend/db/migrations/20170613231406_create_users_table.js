
exports.up = function(knex, Promise) {
  return knex.schema.createTable('users', (table) => {
    table.increments('id').primary();
    table.string('name');
    table.string('email').unique();
    table.string('password');
    table.string('api_token').unique();
    table.timestamps();
  }).createTable('tests', function(table) {
    // just testing the chaining of multiple tables
    table.increments('id').primary();
    table.string('details');
    table.integer('user_id')
      .unsigned()
      .references('users.id');
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.table('tests', function(table) {
      table.dropForeign('user_id');
      table.dropColumn('user_id');
    })
    .dropTable('users')
    .dropTable('tests');
};
